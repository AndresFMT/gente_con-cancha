import React from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter as Router } from 'react-router-dom'
import createStore from '../../modules/core/src/config/store'
const { store } = createStore();
const router = storyFn => (
  <Provider store={store}>
    <Router>
            {storyFn()}
    </Router>
  </Provider>
)

export default router
