/* eslint-disable no-use-before-define */
import React from 'react';
import PropTypes from 'prop-types';
import ChapterBox from '../chapterBox';
import LandingModal from '../Landingmodal';
import SliderSlick from '../slider-slick';
import { BaseCarousel } from './styled';


const SliderCard = ({ items, onVideoSelected, backTheme }) => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slide: 'span',
    slidesToShow: 1,
    fade: true,
    cssEase: 'linear',
    pauseOnHover: false,
    autoplay: true,
    autoplaySpeed: 5000,
    arrows: false,
    accessibility: false,
    lazyLoad: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4,
          arrows: false,
        },
      },
    ],
    appendDots: () => (
      <div
        style={{
          backgroundColor: 'transparent',
          display: 'inline-flex',
          padding: '10px',
          position: 'absolute',
          bottom: '10%',
          width: '100%',
        }}
      >
        <ul
          style={{
            margin: '0 auto',
            width: '100%',
            display: 'inline-flex',
            maxWidth: '1200px',
          }}
        >
          {renderBox(items)}
        </ul>
      </div>
    ),
    customPaging: i => (
      <div
        style={{
          width: '30px',
          color: 'blue',
          border: '1px blue solid',
        }}
      >
        {i}
      </div>
    ),
  };

  const renderBox = boxes => boxes.map((box, indx) => (
    <LandingModal
      // eslint-disable-next-line react/no-array-index-key
      key={indx}
      itemLanding={box}
    />
  ));

  const renderCards = cards => cards.map((card, indx) => (
    <ChapterBox
      // eslint-disable-next-line react/no-array-index-key
      key={indx}
      item={card}
      onVideoSelected={onVideoSelected}
      backTheme={backTheme}
      initialState={false}
    />
  ));

  // const renderFigure = figure => figure.map((fig, indx) => (
  //   <ChapterFigure
  //     // eslint-disable-next-line react/no-array-index-key
  //     key={indx}
  //     item={fig}
  //   />
  // ));

  return (
    <>
      <BaseCarousel>
        <SliderSlick settings={settings}>
          {renderCards(items)}
        </SliderSlick>
      </BaseCarousel>
    </>
  );
};

SliderCard.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    nid: PropTypes.string,
    title: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
    autor: null,
    body: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
    image: PropTypes.shape({
      src: PropTypes.string,
      alt: PropTypes.string,
      title: PropTypes.string,
    }),
    episode: PropTypes.string,
    lead: PropTypes.string,
    videoType: PropTypes.string,
    video_youtube: PropTypes.string,
    MediaStream: null,
    images: null,
  })).isRequired,
  onVideoSelected: PropTypes.func,
  backTheme: PropTypes.string,
};

SliderCard.defaultProps = {
  backTheme: 'light',
  onVideoSelected: null,
};

SliderCard.displayName = 'SliderCard';

export default SliderCard;
