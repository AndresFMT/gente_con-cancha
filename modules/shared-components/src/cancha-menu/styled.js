import {
  Box, Image, Flex,
} from 'rebass';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import get from 'lodash/get';

export const Root = styled(Flex)`
  z-index: 1;
  font-family: ${props => get(props, 'theme.fonts.sans', '')};
  margin: 0 auto;
  max-width: ${props => get(props, 'theme.breakpoints.3', '')};
  padding: 0 5px;
  max-height: auto;
  text-align: center;
  padding-top: 50px;
  width: 100%;
`;

export const LinkWrapper = styled(Link)`
  background-color: transparent;
  color: ${props => get(props, 'theme.colors.white', '')};
  display: block;
  text-decoration: none;
  position: relative;
`;

export const Card = styled(Box)`
  font-family: ${props => get(props, 'theme.fonts.sans', '')};
  color: ${props => get(props, 'theme.colors.white', '')};
  display: inline-flex;
  text-align: center;
  justify-content: center;
  align-items: flex-start;
`;

export const ImgLogo = styled(Image)`
  display: inline-block;
  max-width: 100%;
  height: 98px;
`;

export const BoxLine = styled(Box)`
  height: 1px;
  border: 1px solid ${props => get(props, 'theme.colors.whiteOpacity30', '')};
`;

export const Boxbase = styled(Box)`
  top: 0;
  left: 0;
  width: 100%;
  z-index: 1;
`;
