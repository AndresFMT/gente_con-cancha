/* eslint-disable no-sequences */
/* eslint-disable react/prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from 'rebass';
import ShareCancha from '../share-cancha';
import ItemList from '../itemList';
import {
  Root, Card, Boxbase,
  LinkWrapper, ImgLogo, BoxLine,
} from './styled';
import logoCancha from '../../../core/public/images/logo-compuesto.svg';

const CanchaMenu = (props) => {
  const {
    order, canchaLInkGol, canchaLinkGente, menuList, menuTheme,
  } = props;
  return (
    <Boxbase className="header">
      <Root flexDirection="row" m="auto" order={order} justifyContent="space-between">
        <Card menuTheme={menuTheme} alignItems="center">
          <ImgLogo src={logoCancha} alt="genteConCancha" mx="auto" />
          <LinkWrapper to={canchaLInkGol} onClick={() => ('CanchaMenu', 'DW - CanchaMenu Gol Caracol', 'Logo Go To Gol Caracol')} />
          <LinkWrapper to={canchaLinkGente} onClick={() => ('CanchaMenu', 'DW - CanchaMenu Home Gente', 'Logo Go To Gol Caracol')} />
        </Card>
        <Flex flexDirection="row" flexWrap="wrap" pl={16} justifyContent="flex-end" width={1}>
          <ItemList
            menuList={menuList}
            key={menuList.shortname}
          />
          <BoxLine
            width={1}
          />
        </Flex>
        <ShareCancha />
      </Root>
    </Boxbase>
  );
};

CanchaMenu.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  canchaLinkGente: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
  canchaLInkGol: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
  order: PropTypes.number,
  menuList: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    shortname: PropTypes.string.isRequired,
  })).isRequired,
  menuTheme: PropTypes.string,
};

CanchaMenu.defaultProps = {
  canchaLinkGente: 'https://www.genteconcancha.com/',
  canchaLInkGol: 'https://gol.caracoltv.com/',
  order: 0,
  menuTheme: 'default',
};

export default CanchaMenu;
