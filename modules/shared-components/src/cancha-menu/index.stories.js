import React from 'react';
import { storiesOf } from '@storybook/react';
import CanchaMenu from '.';
import logo from '../../../core/public/images/logo-compuesto.svg';

const menuList = [
  {
    title: 'Manifesto',
    shortname: 'manifesto',
    url: 'https://www.genteconcancha.com/manifesto',
  },
  {
    title: 'Capítulos',
    shortname: 'capitulos',
    url: 'https://www.genteconcancha.com/capitulos',
  },
  {
    title: 'Galería',
    shortname: 'galeria',
    url: 'https://www.genteconcancha.com/galeria',
  },
  {
    title: 'Extras',
    shortname: 'extras',
    url: 'https://www.genteconcancha.com/extras',
  },
  {
    title: 'Perfíles',
    shortname: 'perfiles',
    url: 'https://www.genteconcancha.com/perfiles',
  },
  {
    title: 'Créditos',
    shortname: 'creditos',
    url: 'https://www.genteconcancha.com/creditos',
  },
];

const logoCancha = logo;
const canchaLinkGente = 'https://www.genteconcancha.com/';
const canchaLInkGol = 'https://gol.caracoltv.com/';

storiesOf('CanchaMenu', module)
  .add('default', () => (
    <CanchaMenu
      logoCancha={logoCancha}
      canchaLinkGente={canchaLinkGente}
      canchaLInkGol={canchaLInkGol}
      menuList={menuList}
    />
  ));
