import {
  Flex, Box, Heading, Card,
} from 'rebass';
import styled from 'styled-components';
import get from 'lodash/get';

const getGradient = props => (props.showShow ? `linear-gradient(180deg, ${get(props, 'theme.colors.black', '')} 20%,
${get(props, 'theme.colors.darkAlpha', '')} 80%)` : '');

export const Wrapper = styled(Flex)`
  font-family: ${props => get(props, 'theme.fonts.sans', '')};
  flex-direction: column;
  text-decoration: none;
  background: ${props => getGradient(props)};
  position: relative;
  width: 244px;
  align-items: center;
`;

export const BoxContent = styled(Box)`
  position: relative;
  max-width: ${props => get(props, 'theme.breakpoints.3', '1440px')};
  margin: 0 auto;

`;

export const BoxVideo = styled(Box)`
  position: relative;
`;

export const Modal = styled(Box)`
  .modal-overlay {
    position: absolute;
  }
`;

export const ImageIcon = styled(Box)`
    height: 20px;
    position: absolute;
    top: -25px;
    right: -25px;
    cursor: pointer;
    svg {
      height: 20px;
      width: 20px;
      transition: all 0.3s ease-in-out 0s;
      &:hover {
        transform: rotate(360deg);
        transition: all 0.3s ease-in-out 0s;
      }
      path {
        fill: ${props => get(props, 'theme.colors.white', '')};
      }
    }
`;

export const VideoIcon = styled(Box)`
  border-radius: 100%;
  left: 0;
  top: 0;
  padding: 8px;
  position: absolute;

  &:before {
    content: '';
    background-image: url(${props => get(props, 'theme.icons.playerchapter', '')});
    background-size: cover;
    display: block;
    height: 30px;
    width: 30px;
  }
`;

export const BoxIcon = styled(Box)`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 70vh;
  max-width: ${props => get(props, 'theme.breakpoints.3', '1440px')};
  margin: 0 auto;
  ${VideoIcon} {
    position: relative;
    &:before {
      height: 80px;
      width: 80px;
    }
  }
`;

export const PlayerContent = styled(Card)`
  max-width: 1355px;
  width: 100%;
  margin: 0 auto;
  height: 755px;
`;

export const BoxImage = styled(Box)`
  position: relative;
  max-width: 166px;
  height: 98px;
  img {
    max-width: inherit;
    border: 1px solid ${props => get(props, 'theme.colors.white', '')};
    width: 166px;
    height: 98px;
  }
`;

export const Title = styled(Heading)`
  color: ${props => (props.backTheme === 'dark'
    ? get(props, 'theme.colors.white', '')
    : get(props, 'theme.colors.white', ''))};
  font-size: ${props => get(props, 'theme.fontSizes[4]', '') + 2}px;
`;

export const Description = styled.p`
  color: ${props => (props.backtheme === 'dark'
    ? get(props, 'theme.colors.black', '')
    : get(props, 'theme.colors.white', ''))};
  display: ${props => (props.sponsor ? 'none' : 'block')};
  font-size: ${props => get(props, 'theme.fontSizes[2]', '')}px;
  line-height: 24px;
  margin: ${props => get(props, 'theme.space[0]', '')}px;
  text-decoration: none;
`;
