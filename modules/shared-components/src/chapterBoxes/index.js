import React from 'react';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import PropTypes from 'prop-types';
import { Text, Box } from 'rebass';
import { BoxBase } from '../../../core/src/utils/styled';

import {
  Wrapper, BoxImage,
  VideoIcon, BoxContent, Title,
} from './styled';

const chapterBoxes = ({
  // eslint-disable-next-line react/prop-types
  item,
}) => {
  const content = (
    <Box>
      <Title>{item.title}</Title>
    </Box>
  );
  return (
    <BoxBase flexWrap="wrap">
      <BoxContent
        width={1}
      >
        <Wrapper>
          <BoxImage>
            <LazyLoadImage effect="opacity" src={item.image.src} alt={item.title} />
            <VideoIcon />
          </BoxImage>
          <Text flexDirection="column" pt="2" width={1} pl="40px" textAlign="left">{content}</Text>
        </Wrapper>
      </BoxContent>
    </BoxBase>
  );
};

chapterBoxes.propTypes = {
  item: PropTypes.shape({
    nid: PropTypes.string,
    title: PropTypes.string,
    autor: null,
    body: PropTypes.string,
    image: PropTypes.shape({
      src: PropTypes.string,
      alt: PropTypes.string,
      title: PropTypes.string,
    }),
    episode: PropTypes.string,
    lead: PropTypes.string,
    videoType: PropTypes.string,
    video_youtube: PropTypes.string,
    MediaStream: null,
    images: null,
  }).isRequired,
};

chapterBoxes.defaultProps = {

};

export default chapterBoxes;
