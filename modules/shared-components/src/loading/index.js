import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  HomeLoader,
  ChannelLoader,
  ShowLoader,
  VideoLoader,
  VideoPremierLoader,
  DefaultLoader,
} from './pages-loader';

const breakPoint = 768;

const Loading = ({ type, primaryColor, secondaryColor }) => {
  const [isBig, setIsBig] = useState(true);

  useEffect(() => {
    const handleResize = () => setIsBig(window.innerWidth >= breakPoint);
    window.addEventListener('resize', handleResize);
    handleResize();
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);
  const baseOptions = {
    primaryColor,
    secondaryColor,
    speed: 3,
  };
  switch (type) {
    case 'channel':
      return <ChannelLoader baseOptions={baseOptions} isBig={isBig} />;
    case 'home':
      return <HomeLoader baseOptions={baseOptions} isBig={isBig} />;
    case 'show':
      return <ShowLoader baseOptions={baseOptions} isBig={isBig} />;
    case 'video':
      return <VideoLoader baseOptions={baseOptions} isBig={isBig} />;
    case 'videopremier':
      return <VideoPremierLoader baseOptions={baseOptions} isBig={isBig} />;
    default:
      return <DefaultLoader baseOptions={baseOptions} isBig={isBig} />;
  }
};

Loading.propTypes = {
  type: PropTypes.string,
  primaryColor: PropTypes.string,
  secondaryColor: PropTypes.string,
};

Loading.defaultProps = {
  type: '',
  primaryColor: '#e83338',
  secondaryColor: '#032f70',
};

export default Loading;
