/* eslint-disable import/prefer-default-export */
import { Box } from 'rebass';
import styled from 'styled-components';
import get from 'lodash/get';

export const LoaderContainer = styled(Box)`
  max-width: ${props => get(props, 'theme.breakpoints.3', '')};
  margin: 0 auto;
`;
