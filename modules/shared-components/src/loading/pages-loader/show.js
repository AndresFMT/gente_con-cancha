import React from 'react';
import PropTypes from 'prop-types';
import ContentLoader from 'react-content-loader';
import { LoaderContainer } from './styled';
import { Gradient } from '../../../utils/styled';

const bigContentLoader = baseOptions => (
  <ContentLoader
    {...baseOptions}
    height={1350}
    width={1440}
  >
    <rect x="0" y="0" rx="0" ry="0" width="1440" height="142" />
    <rect x="100" y="160" rx="0" ry="0" width="1250" height="550" />
    <rect x="0" y="732" rx="0" ry="0" width="1440" height="142" />
    <rect x="109" y="960" rx="0" ry="0" width="296" height="168" />
    <rect x="110" y="1138" rx="0" ry="0" width="261" height="30" />
    <rect x="419" y="1139" rx="0" ry="0" width="261" height="30" />
    <rect x="109" y="915" rx="0" ry="0" width="265" height="29" />
    <rect x="0" y="1206" rx="0" ry="0" width="1440" height="142" />
    <rect x="112" y="1684" rx="0" ry="0" width="528" height="44" />
    <rect x="418" y="960" rx="0" ry="0" width="296" height="168" />
  </ContentLoader>
);

const contentLoader = baseOptions => (
  <ContentLoader
    {...baseOptions}
    height={1200}
    width={400}
  >
    <rect x="0" y="0" rx="0" ry="0" width="400" height="50" />
    <rect x="0" y="60" rx="0" ry="0" width="265" height="29" />
    <rect x="0" y="105" rx="0" ry="0" width="296" height="168" />
    <rect x="310" y="105" rx="0" ry="0" width="296" height="168" />
    <rect x="0" y="290" rx="0" ry="0" width="400" height="50" />
  </ContentLoader>
);

const ShowPageLoader = ({ baseOptions, isBig }) => {
  const content = isBig
    ? bigContentLoader(baseOptions)
    : contentLoader(baseOptions);

  return (
    <Gradient>
      <LoaderContainer>{content}</LoaderContainer>
    </Gradient>
  );
};

ShowPageLoader.propTypes = {
  baseOptions: PropTypes.shape({
    primaryColor: PropTypes.string.isRequired,
    secondaryColor: PropTypes.string.isRequired,
    speed: PropTypes.number.isRequired,
  }).isRequired,
  isBig: PropTypes.bool.isRequired,
};

export default ShowPageLoader;
