import React from 'react';
import PropTypes from 'prop-types';
import ContentLoader from 'react-content-loader';
import { LoaderContainer } from './styled';
import { Gradient } from '../../../utils/styled';

const bigContentLoader = baseOptions => (
  <ContentLoader
    {...baseOptions}
    height={1500}
    width={1440}
  >
    <rect x="0" y="0" rx="0" ry="0" width="1440" height="122" />
    <rect x="100" y="160" rx="0" ry="0" width="1200" height="600" />
  </ContentLoader>
);

const contentLoader = baseOptions => (
  <ContentLoader
    {...baseOptions}
    height={1200}
    width={400}
  >
    <rect x="0" y="0" rx="0" ry="0" width="400" height="50" />
    <rect x="50" y="60" rx="0" ry="0" width="300" height="600" />
  </ContentLoader>
);

const DefaultPageLoader = ({ baseOptions, isBig }) => {
  const content = isBig
    ? bigContentLoader(baseOptions)
    : contentLoader(baseOptions);

  return (
    <Gradient>
      <LoaderContainer>{content}</LoaderContainer>
    </Gradient>
  );
};

DefaultPageLoader.propTypes = {
  baseOptions: PropTypes.shape({
    primaryColor: PropTypes.string.isRequired,
    secondaryColor: PropTypes.string.isRequired,
    speed: PropTypes.number.isRequired,
  }).isRequired,
  isBig: PropTypes.bool.isRequired,
};

export default DefaultPageLoader;
