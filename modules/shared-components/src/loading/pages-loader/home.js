import React from 'react';
import PropTypes from 'prop-types';
import ContentLoader from 'react-content-loader';
import { LoaderContainer } from './styled';
import { Gradient } from '../../../utils/styled';

const bigContentLoader = baseOptions => (
  <ContentLoader
    {...baseOptions}
    height={1500}
    width={1440}
  >
    <rect x="0" y="0" rx="0" ry="0" width="1440" height="142" />
    <rect x="100" y="180" rx="0" ry="0" width="1200" height="600" />
    <rect x="109" y="960" rx="0" ry="0" width="296" height="168" />
    <rect x="109" y="1136" rx="0" ry="0" width="261" height="30" />
    <rect x="417" y="1136" rx="0" ry="0" width="261" height="30" />
    <rect x="417" y="960" rx="0" ry="0" width="296" height="168" />
    <rect x="1033" y="960" rx="0" ry="0" width="296" height="168" />
    <rect x="724" y="960" rx="0" ry="0" width="296" height="168" />
    <rect x="724" y="1136" rx="0" ry="0" width="261" height="30" />
    <rect x="1032" y="1136" rx="0" ry="0" width="261" height="30" />
    <rect x="109" y="915" rx="0" ry="0" width="265" height="29" />
    <rect x="110" y="1206" rx="0" ry="0" width="794" height="470" />
    <rect x="112" y="1684" rx="0" ry="0" width="528" height="44" />
    <rect x="931" y="1206" rx="0" ry="0" width="399" height="522" />
  </ContentLoader>
);

const contentLoader = baseOptions => (
  <ContentLoader
    {...baseOptions}
    height={1200}
    width={400}
  >
    <rect x="0" y="0" rx="0" ry="0" width="400" height="50" />
    <rect x="50" y="60" rx="0" ry="0" width="300" height="300" />
    <rect x="0" y="418" rx="0" ry="0" width="400" height="102" />
    <rect x="0" y="605" rx="0" ry="0" width="296" height="168" />
    <rect x="310" y="605" rx="0" ry="0" width="296" height="168" />
    <rect x="0" y="560" rx="0" ry="0" width="265" height="29" />
  </ContentLoader>
);

const HomePageLoader = ({ baseOptions, isBig }) => {
  const content = isBig
    ? bigContentLoader(baseOptions)
    : contentLoader(baseOptions);

  return (
    <Gradient>
      <LoaderContainer>{content}</LoaderContainer>
    </Gradient>
  );
};

HomePageLoader.propTypes = {
  baseOptions: PropTypes.shape({
    primaryColor: PropTypes.string.isRequired,
    secondaryColor: PropTypes.string.isRequired,
    speed: PropTypes.number.isRequired,
  }).isRequired,
  isBig: PropTypes.bool.isRequired,
};

export default HomePageLoader;
