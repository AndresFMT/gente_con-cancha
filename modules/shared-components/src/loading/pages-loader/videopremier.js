import React from 'react';
import PropTypes from 'prop-types';
import ContentLoader from 'react-content-loader';
import { LoaderContainer } from './styled';
import { Gradient } from '../../../utils/styled';

const bigContentLoader = baseOptions => (
  <ContentLoader
    {...baseOptions}
    height={1500}
    width={1440}
    speed={3}
  >
    <rect x="0" y="0" rx="0" ry="0" width="1440" height="142" />
    <rect x="433" y="160" rx="0" ry="0" width="728" height="70" />
    <rect x="100" y="250" rx="0" ry="0" width="1200" height="800" />
    <rect x="5" y="946" rx="0" ry="0" width="312" height="289" />
    <rect x="433" y="1311" rx="0" ry="0" width="728" height="90" />
    <rect x="338" y="947" rx="0" ry="0" width="296" height="168" />
    <rect x="339" y="1126" rx="0" ry="0" width="250" height="31" />
  </ContentLoader>
);

const contentLoader = baseOptions => (
  <ContentLoader
    {...baseOptions}
    height={1200}
    width={400}

  >
    <rect x="0" y="0" rx="0" ry="0" width="400" height="50" />
    <rect x="40" y="60" rx="0" ry="0" width="320" height="50" />
    <rect x="0" y="123" rx="0" ry="0" width="400" height="200" />
    <rect x="1" y="340" rx="0" ry="0" width="212" height="127" />
    <rect x="231" y="340" rx="0" ry="0" width="163" height="31" />
  </ContentLoader>
);

const PremierPageLoader = ({ baseOptions, isBig }) => {
  const content = isBig
    ? bigContentLoader(baseOptions)
    : contentLoader(baseOptions);

  return (
    <Gradient>
      <LoaderContainer>{content}</LoaderContainer>
    </Gradient>
  );
};

PremierPageLoader.propTypes = {
  baseOptions: PropTypes.shape({
    primaryColor: PropTypes.string.isRequired,
    secondaryColor: PropTypes.string.isRequired,
    speed: PropTypes.number.isRequired,
  }).isRequired,
  isBig: PropTypes.bool.isRequired,
};

export default PremierPageLoader;
