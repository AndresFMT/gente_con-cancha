import React from 'react';
import PropTypes from 'prop-types';
import ContentLoader from 'react-content-loader';
import { Box } from 'rebass';
import { LoaderContainer } from './styled';

const bigContentLoader = baseOptions => (
  <ContentLoader
    {...baseOptions}
    height={1100}
    width={1240}
  >
    <rect x="0" y="0" rx="0" ry="0" width="1440" height="142" />
    <rect x="19" y="195" rx="4" ry="4" width="1210" height="32" />
    <rect x="19" y="240" rx="4" ry="4" width="850" height="20" />
    <rect x="20" y="287" rx="0" ry="0" width="740" height="416" />
    <rect x="779" y="313" rx="0" ry="0" width="198" height="108" />
    <rect x="993" y="313" rx="0" ry="0" width="220" height="40" />
    <rect x="779" y="433" rx="0" ry="0" width="198" height="108" />
    <rect x="991" y="433" rx="0" ry="0" width="220" height="40" />
    <rect x="779" y="551" rx="0" ry="0" width="198" height="108" />
    <rect x="993" y="551" rx="0" ry="0" width="220" height="40" />
    <rect x="24" y="864" rx="0" ry="0" width="338" height="23" />
    <rect x="28" y="915" rx="0" ry="0" width="296" height="168" />
    <rect x="29" y="1095" rx="0" ry="0" width="296" height="35" />
    <rect x="341" y="915" rx="0" ry="0" width="296" height="168" />
    <rect x="341" y="1095" rx="0" ry="0" width="296" height="35" />
    <rect x="652" y="915" rx="0" ry="0" width="296" height="168" />
    <rect x="652" y="1095" rx="0" ry="0" width="296" height="35" />
    <rect x="961" y="915" rx="0" ry="0" width="296" height="168" />
    <rect x="960" y="1095" rx="0" ry="0" width="296" height="35" />
    <rect x="370" y="75" rx="0" ry="0" width="489" height="63" />
    <rect x="368" y="745" rx="0" ry="0" width="489" height="63" />
  </ContentLoader>
);

const contentLoader = baseOptions => (
  <ContentLoader
    {...baseOptions}
    height={1200}
    width={400}
  >
    <rect x="0" y="0" rx="0" ry="0" width="400" height="50" />
    <rect x="9" y="155" rx="4" ry="4" width="641" height="32" />
    <rect x="10" y="200" rx="0" ry="0" width="740" height="216" />
    <rect x="9" y="427" rx="0" ry="0" width="198" height="108" />
    <rect x="222" y="427" rx="0" ry="0" width="158" height="40" />
    <rect x="10" y="550" rx="0" ry="0" width="198" height="108" />
    <rect x="222" y="550" rx="0" ry="0" width="156" height="40" />
    <rect x="10" y="675" rx="0" ry="0" width="198" height="108" />
    <rect x="221" y="675" rx="0" ry="0" width="156" height="40" />
    <rect x="9" y="900" rx="0" ry="0" width="338" height="23" />
    <rect x="10" y="938" rx="0" ry="0" width="334" height="190" />
    <rect x="10" y="1146" rx="0" ry="0" width="296" height="32" />
    <rect x="43" y="75" rx="0" ry="0" width="320" height="50" />
    <rect x="48" y="815" rx="0" ry="0" width="320" height="50" />
    <rect x="366" y="938" rx="0" ry="0" width="53" height="190" />
    <rect x="366" y="1146" rx="0" ry="0" width="41" height="32" />
  </ContentLoader>
);

const VideoPageLoader = ({ baseOptions, isBig }) => {
  const content = isBig
    ? bigContentLoader(baseOptions)
    : contentLoader(baseOptions);

  return (
    <Box>
      <LoaderContainer>{content}</LoaderContainer>
    </Box>
  );
};

VideoPageLoader.propTypes = {
  baseOptions: PropTypes.shape({
    primaryColor: PropTypes.string.isRequired,
    secondaryColor: PropTypes.string.isRequired,
    speed: PropTypes.number.isRequired,
  }).isRequired,
  isBig: PropTypes.bool.isRequired,
};

export default VideoPageLoader;
