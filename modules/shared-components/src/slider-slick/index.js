import React from 'react';
import PropTypes from 'prop-types';
import { BoxSlider, SliderClass } from './styled';

import { trackGallery, trackSimpleEvent } from '../../utils/analytics';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

const TrackClick = (props, event) => {
  let currentSlider = props.currentSlide;

  if (event === 'next') {
    currentSlider += 2;
  }

  let action = `Slide Advance - ${currentSlider}`;

  if (currentSlider === 1) {
    action = `Initiate - ${currentSlider}`;
  } else if (currentSlider === props.slideCount) {
    action = `Finish - ${currentSlider}`;
  }

  if (props.type === 'gallery') {
    trackGallery(`DW - ${action}`, props.title);
  }

  if (props.isCarousel) {
    currentSlider = (props.currentSlide / 4) + 2;
  }

  trackSimpleEvent((props.type || props.title), `DW - Slider ${currentSlider}`, event);
};

const NextArrow = (props) => {
  const {
    // eslint-disable-next-line react/prop-types
    className, style, onClick,
  } = props;

  return (
    <button
      type="button"
      className={className}
      style={{ ...style }}
      onClick={onClick && (() => { onClick(); TrackClick(props, 'next'); })}
    >
      {'Next'}
    </button>
  );
};

const PrevArrow = (props) => {
  const {
    // eslint-disable-next-line react/prop-types
    className, style, onClick,
  } = props;
  return (
    <button
      type="button"
      className={className}
      style={{ ...style }}
      onClick={onClick && (() => { onClick(); TrackClick(props, 'prev'); })}
    >
      {'Prev'}
    </button>
  );
};

const SliderSlick = ({
  settings,
  children,
  afterChange,
  type,
  isCarousel,

}) => {
  // if (settings.arrows) {
  // eslint-disable-next-line max-len
  // settings.nextArrow = <NextArrow className="slick-arrow slick-next" title={title} type={type} isCarousel={isCarousel} />;
  // eslint-disable-next-line max-len
  // settings.prevArrow = <PrevArrow className="slick-arrow slick-prev" title={title} type={type} isCarousel={isCarousel} />;
  // }

  if (settings.arrows) {
    settings.nextArrow = <NextArrow className="slick-arrow slick-next" type={type} isCarousel={isCarousel} />;
    settings.prevArrow = <PrevArrow className="slick-arrow slick-prev" type={type} isCarousel={isCarousel} />;
  }

  return (
    <BoxSlider>
      <SliderClass {...settings} afterChange={afterChange}>
        {children}
      </SliderClass>
    </BoxSlider>
  );
};

SliderSlick.propTypes = {
  children: PropTypes.node.isRequired,
  settings: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]).isRequired,
  afterChange: PropTypes.func,
  type: PropTypes.string,
  isCarousel: PropTypes.bool,
};

SliderSlick.defaultProps = {
  afterChange: () => {},
  type: '',
  isCarousel: false,
};

SliderSlick.displayName = 'SliderSlick';

export default SliderSlick;
