import { Box } from 'rebass';
import styled from 'styled-components';
import Slider from 'react-slick';
import get from 'lodash/get';


export const SliderClass = styled(Slider)`
  .slick-dots {
      & .slick-active {
      & button {
          background: ${props => get(props, 'theme.colors.discovery')};
        }
      }
    }
  }
`;

// eslint-disable-next-line import/prefer-default-export
export const BoxSlider = styled(Box)`
  .slick-prev {
    background-image: url(${props => get(props, 'theme.icons.slidearrowleftfilled', '')});
    z-index: 99;

    &:hover, &:focus {
      background-image: url(${props => get(props, 'theme.icons.slidearrowleftfilled', '')});
    }
  }
  .slick-next {
    background-image: url(${props => get(props, 'theme.icons.slidearrowrightfilled', '')});
    &:hover, &:focus {
      background-image: url(${props => get(props, 'theme.icons.slidearrowrightfilled', '')});
    }
  }
  .slick-prev, .slick-next {
    background-repeat: no-repeat;
    &.slick-disabled {
      opacity: 0;
    }
    &:before {
      display: none;
    }
    &:hover, &:focus {
      background-repeat: no-repeat;
    }
  }
`;
