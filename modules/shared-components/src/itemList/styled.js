import {
  Box, Link as RebassLink,
} from 'rebass';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import get from 'lodash/get';

export const ExternalLinkWrapper = styled(RebassLink)`
  background-color: transparent;
  text-transform: uppercase;
  color: ${props => get(props, 'theme.colors.white', '')};
  font-size: ${props => get(props, 'theme.fontSizes.1', '')}px;
  display: flex;
  align-items: center;
  text-decoration: none;
  position: relative;
  line-height: 0;
  max-width: 100px;
  width: auto;
`;

export const MenuList = styled.li`
  font-family: ${props => get(props, 'theme.fonts.sans', '')};
  color: ${props => get(props, 'theme.colors.white', '')};
  display: inline-flex;
  text-align: center;
  justify-content: center;
  align-items: center;
  width: 113px;
  height: 100%;
  text-transform: uppercase;
  &:hover {
    a {
      color: ${props => get(props, 'theme.colors.strawberry', '')};
      text-decoration: none;
    }
  }
  &:nth-child(6):after {
    content: "";
    width: 113px;
    height: 3px;
    position: absolute;
    background: red;
    bottom: -2px;
    left: 0;
    transform: translateX(0);
    transition: .5s;
  }
  &:nth-child(2):hover ~ &:nth-child(6):after {
    transform: translateX(113px);
  }
  &:nth-child(3):hover ~ &:nth-child(6):after {
    transform: translateX(226px);
  }
  &:nth-child(4):hover ~ &:nth-child(6):after {
    transform: translateX(339px);
  }
  &:nth-child(5):hover ~ &:nth-child(6):after {
    transform: translateX(452px);
  }
  &:nth-child(6):hover:after {
    transform: translateX(565px);
  }

`;

export const ContentBox = styled(Box)`
  display: inline-flex;
  height: auto;
  position: relative;
`;

export const LinkWrapper = styled(Link)`
  background-color: transparent;
  color: ${props => get(props, 'theme.colors.white', '')};
  display: block;
  text-decoration: none;
  position: relative;
`;
