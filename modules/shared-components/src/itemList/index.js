import React from 'react';
import PropTypes from 'prop-types';
import {
  LinkWrapper,
  ExternalLinkWrapper,
  ContentBox, MenuList,
} from './styled';

export const ItemList = ({
  menuList,
// eslint-disable-next-line arrow-body-style
}) => {
  return (
    <ContentBox>
      {menuList && menuList.length > 0 && menuList.map((list) => {
        const baseWrapper = {
          title: list.title,
          // eslint-disable-next-line no-sequences
          onClick: () => ('Header', 'DW - Header Lower', `Logo Go To Channel - ${list.url}`),
        };
        let content = <LinkWrapper to={list.url}>{list.title}</LinkWrapper>;
        if (list.url.includes('.com')) {
          content = (
            <ExternalLinkWrapper
              {...baseWrapper}
              href={list.url}
            >
              {list.title}
            </ExternalLinkWrapper>
          );
        }
        // eslint-disable-next-line max-len
        return <MenuList className="hover" key={list.shortname} p={0}>{content}</MenuList>;
      })}
    </ContentBox>
  );
};

ItemList.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  menuList: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    shortname: PropTypes.string.isRequired,
  })),
};

ItemList.defaultProps = {
  menuList: [],
};

export default ItemList;
