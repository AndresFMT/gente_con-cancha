import React from 'react';
import { storiesOf } from '@storybook/react';
import ItemList from '.';

const menuList = [
  {
    title: 'Manifesto',
    shortname: 'manifesto',
    url: 'https://www.genteconcancha.com/manifesto',
  },
  {
    title: 'Capítulos',
    shortname: 'capitulos',
    url: 'https://www.genteconcancha.com/capitulos',
  },
  {
    title: 'Galería',
    shortname: 'galeria',
    url: 'https://www.genteconcancha.com/galeria',
  },
  {
    title: 'Extras',
    shortname: 'extras',
    url: 'https://www.genteconcancha.com/extras',
  },
  {
    title: 'Perfíles',
    shortname: 'perfiles',
    url: 'https://www.genteconcancha.com/perfiles',
  },
  {
    title: 'Créditos',
    shortname: 'creditos',
    url: 'https://www.genteconcancha.com/creditos',
  },
];

storiesOf('ItemList', module)
  .add('default item list', () => (
    <ItemList
      menuList={menuList}
    />
  ));
