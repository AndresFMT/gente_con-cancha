import React from 'react';
import PropTypes from 'prop-types';
import {
  Root,
  ImageWrapper,
  HeaderImage,
  ContentWrapper,
  Description,
} from './styled';

const GalleryCard = ({
  card, backtheme,
}) => (
  <Root>
    <ImageWrapper>
      <HeaderImage effect="opacity" src={card.src} alt={card.src} />
    </ImageWrapper>
    <ContentWrapper>
      {card.title
        && (
          <Description backtheme={backtheme}>
            {card.title}
          </Description>
        )
      }
    </ContentWrapper>
  </Root>
);

GalleryCard.propTypes = {
  card: PropTypes.shape({
    src: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
  }).isRequired,
  backtheme: PropTypes.string,
};

GalleryCard.defaultProps = {
  backtheme: 'light',
};

export default GalleryCard;
