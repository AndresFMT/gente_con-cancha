import React from 'react';
import { storiesOf } from '@storybook/react';

import GalleryCard from '.';

const images = {
  src: 'https://dev.gol.caracoltv.com/sites/default/files/1000x500negra_0.png',
  alt: 'Texto alternativo',
  title: 'Título',
};

storiesOf('Gallery Card', module)
  .add('with Image', () => (
    <GalleryCard card={images} backtheme="dark" />
  ), {
    backgrounds: [{
      name: 'Dark', value: 'rgba(0,14,38)', default: true,
    }],
  });
