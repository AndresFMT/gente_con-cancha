import { LazyLoadImage } from 'react-lazy-load-image-component';
import styled from 'styled-components';
import { Flex, Box, Heading } from 'rebass';
import get from 'lodash/get';

const imgStyle = {
  display: 'block',
  width: '100%',
};
export const Root = styled(Box)`
  font-family: ${props => get(props, 'theme.fonts.sans', '')};
  position: relative;
`;

export const HeaderImage = styled(LazyLoadImage)`
  ${imgStyle};
`;

export const Header = styled(Box)`
  position: relative;
  & img {
    ${imgStyle};
  }
`;

export const ChannelSVG = styled(Box)`
  svg {
    height: 30px;
    opacity: 0.3;

    path {
      fill: ${props => (props.backtheme === 'light'
    ? get(props, 'theme.colors.white', '')
    : get(props, 'theme.colors.black', ''))};
    }
  }
`;

export const ContentWrapper = styled(Flex)`
  bottom: ${props => get(props, 'theme.space[0]', '')}px;
  flex-direction: column;
  padding: ${props => get(props, 'theme.space[2]', '16')}px 0;

  & a {
    text-decoration: none;
  }
`;

export const HeadlineWrapper = styled(Box)`
  display: block;
`;

export const Title = styled(Heading)`
  color: ${props => (props.backtheme === 'dark'
    ? get(props, 'theme.colors.black', '')
    : get(props, 'theme.colors.white', ''))};
  font-size: ${props => get(props, 'theme.fontSizes[6]', '')}px;
  line-height: 36px;
  margin: ${props => get(props, 'theme.space[1]', '')}px 0;
  text-decoration: none;
`;

export const Description = styled.p`
  color: ${props => (props.backtheme === 'dark'
    ? get(props, 'theme.colors.black', '')
    : get(props, 'theme.colors.white', ''))};
  display: ${props => (props.sponsor ? 'none' : 'block')};
  font-size: ${props => get(props, 'theme.fontSizes[2]', '')}px;
  line-height: 24px;
  margin: ${props => get(props, 'theme.space[0]', '')}px;
  text-decoration: none;
`;

export const ImageWrapper = styled.div`
  position: relative;
`;
