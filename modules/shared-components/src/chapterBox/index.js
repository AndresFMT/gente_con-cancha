/* eslint-disable react/button-has-type */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import ReactPlayer from 'react-player';
import Modal from 'react-responsive-modal';
import ReactSVG from 'react-svg';
import ChapterFigure from '../chapterFigure';
import TuneInOpening from '../tune-in-opening';
import CloseModal from '../../icons/closeModal.svg';
import { BoxBase } from '../../../core/src/utils/styled';


import {
  BoxImage, HeadingTitle,
  VideoIcon, ImageIcon,
  BoxIcon, BoxVideo,
  PlayerContent,
} from './styled';

const ChapterBox = ({ item, onVideoSelected }) => {
  const [event, setEvent] = useState(false);
  const [clicked, setClicked] = useState(true);
  const hide = () => {
    setClicked(!clicked);
    setEvent(false);
  };

  return (
    <>
      <BoxBase flexWrap="wrap">
        <TuneInOpening videoURL={item.lead} />
        <Modal
          classNames={{
            overlay: 'modal-overlay',
            modal: 'modal',
            closeButton: 'modal-close-button',
          }}
          open={event}
          onClose={hide}
          showCloseIcon={false}
          focusTrapped={false}
          blockScroll={false}
          styles={{
            modal: {
              maxWidth: '100%',
              width: '100%',
              padding: 'unset',
            },
            overlay: {
              background: 'rgba(0, 0, 0, 0.5)',
              position: 'absolute',
            },
          }}
          animationDuration={1000}
          onVideoSelected={onVideoSelected}
          center
        >
          <BoxVideo width={1}>
            <PlayerContent>
              <ReactPlayer
                className="react-player"
                width="100%"
                height="100%"
                url={clicked ? item.video_youtube : ''}
                // url="https://www.youtube.com/watch?v=kI4dUyuvPTk"
                // eslint-disable-next-line react/jsx-boolean-value
                playing={event}
              />
              <ImageIcon onClick={hide}>
                <ReactSVG src={CloseModal} />
              </ImageIcon>
            </PlayerContent>
          </BoxVideo>
          <ChapterFigure
            // eslint-disable-next-line react/no-array-index-key
            item={item}
          />
        </Modal>
        <BoxIcon
          width={1}
          onClick={() => { setEvent(true); setClicked(true); }}
        >
          <BoxImage>
            <VideoIcon />
            <HeadingTitle>
              {item.title}
            </HeadingTitle>
          </BoxImage>
        </BoxIcon>
      </BoxBase>
    </>
  );
};

ChapterBox.propTypes = {
  item: PropTypes.shape({
    nid: PropTypes.string,
    title: PropTypes.string,
    autor: null,
    body: PropTypes.string,
    image: PropTypes.shape({
      src: PropTypes.string,
      alt: PropTypes.string,
      title: PropTypes.string,
    }),
    episode: PropTypes.string,
    lead: PropTypes.string,
    videoType: PropTypes.string,
    video_youtube: PropTypes.string,
    MediaStream: null,
    images: null,
  }).isRequired,
  onVideoSelected: PropTypes.func,
};

ChapterBox.defaultProps = {
  onVideoSelected: null,
};

export default ChapterBox;
