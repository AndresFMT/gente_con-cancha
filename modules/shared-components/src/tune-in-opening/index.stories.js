import React from 'react';
import { storiesOf } from '@storybook/react';
import {
  withKnobs,
} from '@storybook/addon-knobs';
import TuneInOpening from '.';

const video = {
  nid: '203038',
  title: 'Gente con cancaha capitulo 4',
  autor: null,
  lead: 'https://dev.gol.caracoltv.com/sites/default/files/videoteaser6segundo.mp4',
  body: '<p>Duis lobortis massa imperdiet quam. Mauris sollicitudin fermentum libero. Ut a nisl id ante tempus hendrerit. Phasellus gravida semper nisi.</p> <p>Praesent porttitor, nulla vitae posuere iaculis, arcu nisl dignissim dolor, a pretium mi sem ut ipsum. Integer tincidunt. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc.</p> <p>Sed augue ipsum, egestas nec, vestibulum et, malesuada adipiscing, dui. Nunc nonummy metus. Ut non enim eleifend felis pretium feugiat. Morbi nec metus.</p> ',
  image: {
    src: 'https://dev.gol.caracoltv.com/sites/default/files/lxuasma_shock_2.jpg',
    alt: '',
    title: '',
  },
  video_youtube: 'https://www.youtube.com/watch?v=kI4dUyuvPTk',
  MediaStream: null,
  images: null,
};

storiesOf('Tune In Opening', module)
  .addDecorator(withKnobs)
  .add('Tune In Opening Video', () => (
    <TuneInOpening
      background="video"
      videoURL={video.lead}
    />
  ));
