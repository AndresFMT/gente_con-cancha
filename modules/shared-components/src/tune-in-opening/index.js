/* eslint-disable react/button-has-type */
import React from 'react';
import PropTypes from 'prop-types';
import {
  Root,
  // TextChannel, TODO AIR TIME
} from './styled';
import VideoBackground from './video-background';

const TuneInOpening = ({
  videoURL,
}) => (
  <Root>
    <VideoBackground videoURL={videoURL} />
  </Root>
);

TuneInOpening.propTypes = {
  videoURL: PropTypes.string,
  track: PropTypes.shape({}),
};

TuneInOpening.defaultProps = {
  videoURL: '/video',
  track: {},
};

export default TuneInOpening;
