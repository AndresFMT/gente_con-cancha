import {
  Box, Flex,
} from 'rebass';
import styled from 'styled-components';
import get from 'lodash/get';

export const Root = styled(Box)`
  font-family: ${props => get(props, 'theme.fonts.sans', '')};
  line-height: 32px;
  margin: 0 auto;
  max-width: ${props => get(props, 'theme.breakpoints.4', '')};
  width: 100%;
  height: 100%;
  overflow: hidden;
  position: absolute;
  transition: all 0.5s ease;

  a {
    text-decoration: none;
  }
`;

export const WrapperTitle = styled(Flex)`
  flex-direction: row;
  align-items: center;

`;

export const VideoContainer = styled(Box)`
  overflow: visible;
  position: relative;
  text-align: center;
  width: 100%;
`;

export const Mute = styled(Box)`
  background:url(${props => (props.muted === true
    ? get(props, 'theme.icons.volumeonfilled', '')
    : get(props, 'theme.icons.volumeofffilled', ''))});
  background-position: center;
  cursor: pointer;
  filter: invert(100%);
  height: 24px;
  position: absolute;
  top: ${props => get(props, 'theme.space[1]', '')}px;
  text-transform: uppercase;
  right: ${props => get(props, 'theme.space[1]', '')}px;
  line-height: 32px;
  width: 24px;
  z-index: 2;
`;

export const Video = styled.video`
  position: fixed;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100vh;
  object-fit: cover;
`;
