/* eslint-disable */
import React from 'react';
import { storiesOf } from '@storybook/react';
import {
  withKnobs,
} from '@storybook/addon-knobs';

import ArticleImage from '.';

const image = {
  src: "https://dev.gol.caracoltv.com/sites/default/files/foto_1.jpg",
  alt: "",
  title: "",
};

storiesOf('ArticleImage', module)
  .addDecorator(withKnobs)
  .add('Dark Theme Default', () =>
    <ArticleImage imageData={image} />, {
      backgrounds: [{
        name: 'black', value: 'rgba(0,14,38)', default: true
      }]
    }
  );
