import React from 'react';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import PropTypes from 'prop-types';
import LazyLoad from 'react-lazyload';
import Root from './styled';

const ArticleImage = ({ imageData }) => (
  <Root>
    <figure>
      <LazyLoad throttle={200} debounce={500} height={window.width < 768 ? 170 : 570}>
        <LazyLoadImage effect="opacity" src={imageData.src} alt={imageData.alt} />
      </LazyLoad>
      {imageData.title && (
        <figcaption>{imageData.title}</figcaption>
      )}
    </figure>
  </Root>
);

ArticleImage.propTypes = {
  imageData: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
};

export default ArticleImage;
