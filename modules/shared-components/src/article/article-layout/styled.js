import {
  Flex, Box, Text,
} from 'rebass';
import styled from 'styled-components';
import get from 'lodash/get';

const ModuleBoxMain = {
  padding: '20px 17px 0',
};

export const Root = styled(Flex)`
  flex-wrap: wrap;
  color: ${props => get(props, 'theme.colors.black', '')};
  max-width: 100%;

  & .article-block {
    width: 100%;
    display: inline-flex;
    justify-content: center;
    flex-wrap: wrap;
    padding-top: 50px;
  }
`;

export const ShareModule = styled(Box)`
  position: relative;
  width: 85%;
  padding-top: 10px;

`;

export const ModuleBox = styled(Box)`
  ${ModuleBoxMain};

  &.related-content {
    margin: 0;

  }

  @media (min-width: ${props => get(props, 'theme.breakpoints.1', '')}) {
    padding: 0;
  }
`;

export const ImageBox = styled(Box)`
  position: relative;
`;

export const DescCard = styled(Box)`
  display: inline-flex;
  width: 90%;
  position: absolute;
  bottom: -7%;
  flex-wrap: wrap;
  background: ${props => get(props, 'theme.colors.bgExtras', '')};
  transform: translate(-50%);
  left: 50%;
  padding: 25px;
  margin-top: 0;

  @media (min-width: ${props => get(props, 'theme.breakpoints.1', '')}) {
    padding: 28px;
  }

  h1 {
    font-family: ${props => get(props, 'theme.fonts.sans2', '')};
    font-size: calc(${props => get(props, 'theme.fontSizes.7', '')}px);
    margin: 0;
    font-weight: bold;
    color: ${props => get(props, 'theme.colors.marineBlue', '')};

    @media (min-width: ${props => get(props, 'theme.breakpoints.1', '')}) {
      font-size: ${props => get(props, 'theme.fontSizes.11', '')}px;
      line-height: 64px;
    }
  }
`;

export const ShortDesc = styled(Text)`
  font-family: ${props => get(props, 'theme.font.NeueBold', '')};
  font-size: ${props => get(props, 'theme.fontSizes.3', '')}px;
  color: ${props => get(props, 'theme.colors.strawberry', '')};
  font-weight: normal;
  width: 85%;
  font-weight: bold;
  span {
    color: ${props => get(props, 'theme.colors.marineBlue', '')};
    font-weight: bold;
    margin-right: 10px;
  }

  @media (min-width: ${props => get(props, 'theme.breakpoints.1', '')}) {
    font-size: calc(${props => get(props, 'theme.fontSizes.3', '')}px - 2px);
    font-weight: 600;
    line-height: 24px;
  }
`;

export const TextBox = styled(Text)`
  font-family: ${props => get(props, 'theme.font.Kalam', '')};
  font-size: calc(${props => get(props, 'theme.fontSizes.3', '')}px - 2px);
  color: ${props => get(props, 'theme.colors.marineBlue', '')};
`;
