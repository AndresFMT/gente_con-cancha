/* eslint-disable no-mixed-operators */
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

// Import components
import isEmpty from 'lodash/isEmpty';
import ArticleBodyData from '../article-bodytext';
import ArticleImage from '../article-image';
import Share from '../../share';
import Loading from '../../loading';
import patterChapterFigure from '../../../../core/public/images/patterChapterFigure.png';


// Helpers

import {
  Root,
  DescCard,
  ShortDesc,
  ModuleBox,
  ImageBox,
  ShareModule,
  TextBox,
} from './styled';

const ArticleLayout = ({
  data, url, shareTitle,
}) => {
  // const [showMobile, setShowMobile] = useState(false);
  // const [showDesktop, setShowDesktop] = useState(false);

  const articleData = data;

  const iconStyle = {
    backgroundImage: `url(${patterChapterFigure})`,
  };

  if (isEmpty(data)) {
    return <Loading type="article" />;
  }

  useEffect(() => {
    // const handleMediaQueryChangeMobile = (event) => {
    //   setShowMobile(!!event.matches);
    // };

    // const handleMediaQueryChangeDesktop = (event) => {
    //   setShowDesktop(!!event.matches);
    // };

    // const mediaQueryList = window.matchMedia('(max-width: 767px)');
    // const mediaQueryListD = window.matchMedia('(min-width: 768px)');

    // // Adding listeners to init media queries with matchMedia
    // mediaQueryList.addListener(handleMediaQueryChangeMobile);
    // mediaQueryListD.addListener(handleMediaQueryChangeDesktop);
    // handleMediaQueryChangeMobile(mediaQueryList);
    // handleMediaQueryChangeDesktop(mediaQueryListD);

    // return () => {
    //   // Removing listeners to media queries with matchMedia
    //   mediaQueryList.removeListener(handleMediaQueryChangeMobile);
    //   mediaQueryListD.removeListener(handleMediaQueryChangeDesktop);
    // };
  });

  return (
    <Root>
      <ImageBox width={1}>
        {(articleData && articleData.image) && (
        <ArticleImage imageData={articleData.image} />
        )}
        <DescCard>
          {(articleData && articleData.title) && <h1>{articleData.title}</h1>}
        </DescCard>
      </ImageBox>
      <section className="article-block" style={iconStyle}>
        {(articleData && articleData.autor)
          && (
          <ShortDesc>
            <span>Por:</span>
            {articleData.autor}
          </ShortDesc>
          )}
        <ShareModule>
          <Share backTheme="light" url={url} shareDirection="row" title={shareTitle} />
        </ShareModule>
        <ModuleBox width={[1, 1, 10 / 12]}>
          <TextBox>{articleData.lead}</TextBox>
          <ArticleBodyData bodyData={articleData.body} />
        </ModuleBox>
      </section>
    </Root>
  );
};

ArticleLayout.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
  url: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
  shareTitle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

ArticleLayout.defaultProps = {
  shareTitle: 'Comparte este artículo',
};

export default ArticleLayout;
