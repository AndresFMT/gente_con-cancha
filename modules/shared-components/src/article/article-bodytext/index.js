import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import renderHTML from 'react-render-html';
import Root from './styled';


const ArticleBodyData = ({ bodyData }) => {
  useEffect(() => {
    const script = document.createElement('script');
    const scriptWidgets = document.createElement('script');

    script.src = '//if-cdn.com/embed.js';
    scriptWidgets.src = 'https://platform.twitter.com/widgets.js';
    script.async = true;
    scriptWidgets.async = true;

    document.body.appendChild(script);
    document.body.appendChild(scriptWidgets);
  }, []);
  return (
    <Root>
      {renderHTML(bodyData)}
    </Root>
  );
};

ArticleBodyData.propTypes = {
  bodyData: PropTypes.string.isRequired,
};

export default ArticleBodyData;
