import React from 'react';
import { storiesOf } from '@storybook/react';
// import { withKnobs } from '@storybook/addon-knobs';

import SliderPerfil from '.';

const content = [
  {
    body: '<p data-pm-slice="1 1 []"><strong>Luis Salcedo es bogotano, tiene 42, años, comenzó a jugar microfútbol desde los 8 años en un sector popular de Bogotá, en el barrio Kennedy. Y desde entonces, el micro ha sido todo en su vida. Así jugó en selecciones Colombia, con la que jugó Copas América en Brasil y Uruguay, Gran Prix en Brasil; jugó profesionalmente en Italia cuatro temporadas y después se fue a China, en donde primero fue jugador y después técnico. Ahora creó un club de micro en su barrio y allí transmite esos conocimientos adquiridos en su carrera. Cuenta con 80 niños entre los 4 y 16 años y se ha convertido en un líder deportivo de su barrio y comunidad.</strong></p> ',
    title: 'Clone of Luis Salcedo1',
    image: {
      src: 'https://dev.gol.caracoltv.com/sites/default/files/perfil-1.jpg',
      alt: '',
      title: '',
    },
  },
  {
    body: '<p data-pm-slice="1 1 []"><strong>Luis Salcedo es bogotano, tiene 42, años, comenzó a jugar microfútbol desde los 8 años en un sector popular de Bogotá, en el barrio Kennedy. Y desde entonces, el micro ha sido todo en su vida. Así jugó en selecciones Colombia, con la que jugó Copas América en Brasil y Uruguay, Gran Prix en Brasil; jugó profesionalmente en Italia cuatro temporadas y después se fue a China, en donde primero fue jugador y después técnico. Ahora creó un club de micro en su barrio y allí transmite esos conocimientos adquiridos en su carrera. Cuenta con 80 niños entre los 4 y 16 años y se ha convertido en un líder deportivo de su barrio y comunidad.</strong></p> ',
    title: 'Clone of Luis Salcedo2',
    image: {
      src: 'https://dev.gol.caracoltv.com/sites/default/files/perfil-1_0.jpg',
      alt: '',
      title: '',
    },
  },
  {
    body: '<p data-pm-slice="1 1 []"><strong>Luis Salcedo es bogotano, tiene 42, años, comenzó a jugar microfútbol desde los 8 años en un sector popular de Bogotá, en el barrio Kennedy. Y desde entonces, el micro ha sido todo en su vida. Así jugó en selecciones Colombia, con la que jugó Copas América en Brasil y Uruguay, Gran Prix en Brasil; jugó profesionalmente en Italia cuatro temporadas y después se fue a China, en donde primero fue jugador y después técnico. Ahora creó un club de micro en su barrio y allí transmite esos conocimientos adquiridos en su carrera. Cuenta con 80 niños entre los 4 y 16 años y se ha convertido en un líder deportivo de su barrio y comunidad.</strong></p> ',
    title: 'Clone of Luis Salcedo3',
    image: {
      src: 'https://dev.gol.caracoltv.com/sites/default/files/perfil-1_1.jpg',
      alt: '',
      title: '',
    },
  },
  {
    body: '<p data-pm-slice="1 1 []"><strong>Luis Salcedo es bogotano, tiene 42, años, comenzó a jugar microfútbol desde los 8 años en un sector popular de Bogotá, en el barrio Kennedy. Y desde entonces, el micro ha sido todo en su vida. Así jugó en selecciones Colombia, con la que jugó Copas América en Brasil y Uruguay, Gran Prix en Brasil; jugó profesionalmente en Italia cuatro temporadas y después se fue a China, en donde primero fue jugador y después técnico. Ahora creó un club de micro en su barrio y allí transmite esos conocimientos adquiridos en su carrera. Cuenta con 80 niños entre los 4 y 16 años y se ha convertido en un líder deportivo de su barrio y comunidad.</strong></p> ',
    title: 'Clone of Luis Salcedo4',
    image: {
      src: 'https://dev.gol.caracoltv.com/sites/default/files/perfil-1_2.jpg',
      alt: '',
      title: '',
    },
  },
  {
    body: '<p data-pm-slice="1 1 []"><strong>Luis Salcedo es bogotano, tiene 42, años, comenzó a jugar microfútbol desde los 8 años en un sector popular de Bogotá, en el barrio Kennedy. Y desde entonces, el micro ha sido todo en su vida. Así jugó en selecciones Colombia, con la que jugó Copas América en Brasil y Uruguay, Gran Prix en Brasil; jugó profesionalmente en Italia cuatro temporadas y después se fue a China, en donde primero fue jugador y después técnico. Ahora creó un club de micro en su barrio y allí transmite esos conocimientos adquiridos en su carrera. Cuenta con 80 niños entre los 4 y 16 años y se ha convertido en un líder deportivo de su barrio y comunidad.</strong></p> ',
    title: 'Clone of Luis Salcedo5',
    image: {
      src: 'https://dev.gol.caracoltv.com/sites/default/files/perfil-1_3.jpg',
      alt: '',
      title: '',
    },
  },
  {
    body: '<p data-pm-slice="1 1 []"><strong>Luis Salcedo es bogotano, tiene 42, años, comenzó a jugar microfútbol desde los 8 años en un sector popular de Bogotá, en el barrio Kennedy. Y desde entonces, el micro ha sido todo en su vida. Así jugó en selecciones Colombia, con la que jugó Copas América en Brasil y Uruguay, Gran Prix en Brasil; jugó profesionalmente en Italia cuatro temporadas y después se fue a China, en donde primero fue jugador y después técnico. Ahora creó un club de micro en su barrio y allí transmite esos conocimientos adquiridos en su carrera. Cuenta con 80 niños entre los 4 y 16 años y se ha convertido en un líder deportivo de su barrio y comunidad.</strong></p> ',
    title: 'Clone of Luis Salcedo6',
    image: {
      src: 'https://dev.gol.caracoltv.com/sites/default/files/perfil-1_4.jpg',
      alt: '',
      title: '',
    },
  },
];

storiesOf('Perfiles', module)
  .add('with Gallery', () => <SliderPerfil sliderContentPerfil={content} />);
