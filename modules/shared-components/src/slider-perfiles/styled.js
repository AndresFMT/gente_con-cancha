import { Box, Flex } from 'rebass';
import styled from 'styled-components';
import get from 'lodash/get';

export const BoxContent = styled(Box)`
  max-width: ${props => get(props, 'theme.breakpoints.3', '')};
  margin: 0 auto;
  position: relative;

  /* .slick-slide {
    > div {
      margin: 0 ${props => get(props, 'theme.space[1]', '')}px;
    }
  } */

  .slick-dots {
    position: relative;
    color: ${props => get(props, 'theme.colors.strawberry', '')};
    font-size: ${props => get(props, 'theme.fontSizes[8]', '')}px;
    & li {
      display: none;
      margin: 0;
      & button {
        background: white;
        border-radius: 8px;
        height: 8px;
        padding: 0;
        width: 8px;
        &:before {
            display: none;
        }
      }
    }

    & li.slick-active {
      display: flex;
      & button {
          font-size: 20px;
          color: red;
          background: ${props => get(props, 'theme.colors.transparent', '')};
      }
    }
  }

  & .slick-arrow {
    height: 56px;
    top: 45%;
    width: 56px;
    z-index: 8;

    &:before {
      display: none;
    }
  }

  & .slick-next {
    background-size: 100%;
    right: -10%;

    &:hover, &:focus {
      background-size: 100%;
      opacity: 0.8;
    }
  }

  & .slick-prev {
    background-size: 100%;
    left: -10%;

    &:hover, &:focus {
      background-size: 100%;
      opacity: 0.8;
    }
  }

  & .slick-disabled {
    opacity: 0;

    &:focus {
      opacity: 0;
    }
  }
`;

export const BaseGalleryPerfil = styled(Flex)`
  height: 100vh;
  align-items: center;
`;
