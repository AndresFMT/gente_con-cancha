import React from 'react';
import PropTypes from 'prop-types';
import map from 'lodash/map';
import isEmpty from 'lodash/isEmpty';
import LoadingComponent from '../loading';
import SliderSlick from '../slider-slick';
import PerfilCard from '../perfil-card';
import { BaseGalleryPerfil, BoxContent } from './styled';
import patterChapterFigure from '../../../core/public/images/patterChapterFigure.png';
import 'react-image-gallery/styles/css/image-gallery.css';

const SliderPerfil = ({
  sliderContentPerfil, backtheme, loading,
}) => {
  if (loading || isEmpty(sliderContentPerfil)) {
    return (
      <LoadingComponent type="sliderContentCardLoader" />
    );
  }

  const iconStyle = {
    backgroundImage: `url(${patterChapterFigure})`,
  };

  const items = map(sliderContentPerfil);
  global.console.log(items.length);

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slide: 'span',
    slidesToShow: 1,
    fade: true,
    cssEase: 'linear',
    pauseOnHover: false,
    autoplay: false,
    arrows: true,
    accessibility: false,
    lazyLoad: true,
    appendDots: (i, index) => (
      <div
        style={{
          backgroundColor: 'transparent',
          display: 'inline-flex',
          justifyContent: 'center',
          padding: '10px',
          position: 'relative',
          width: '100%',
        }}
      >
        <ul
          style={{
            margin: '0',
            width: 'auto',
            display: 'inline-flex',
          }}
        >
          {i}
          {index}
        </ul>
        <div>
          /
          {' '}
          {items.length}
        </div>
      </div>
    ),
    customPaging: i => (
      <div
        style={{
          width: '30px',
        }}
      >
        {i + 1}
      </div>
    ),
  };

  const renderCards = cards => cards.map((card, indx) => (
    <PerfilCard
      // eslint-disable-next-line react/no-array-index-key
      key={indx}
      card={card}
      initialState={false}
    />
  ));

  return (
    <BaseGalleryPerfil backtheme={backtheme} style={iconStyle}>
      <BoxContent>
        <SliderSlick settings={settings}>
          {renderCards(sliderContentPerfil)}
        </SliderSlick>
      </BoxContent>
    </BaseGalleryPerfil>
  );
};

SliderPerfil.propTypes = {
  sliderContentPerfil: PropTypes.arrayOf(PropTypes.shape({
    body: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    image: PropTypes.shape({
      src: PropTypes.string,
      alt: PropTypes.string,
      title: PropTypes.string,
    }),
  })).isRequired,
  backtheme: PropTypes.string,
  track: PropTypes.shape({
    component: PropTypes.string,
  }),
  loading: PropTypes.bool,
};

SliderPerfil.defaultProps = {
  backtheme: 'gallery',
  track: {},
  loading: false,
};

export default SliderPerfil;
