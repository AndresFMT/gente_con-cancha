import React from 'react';
import { storiesOf } from '@storybook/react';
import {
  withKnobs,
} from '@storybook/addon-knobs';
import Particles from '.';

storiesOf('particles', module)
  .addDecorator(withKnobs)
  .add('Particles', () => (
    <Particles />
  ), {
    backgrounds: [{
      name: 'black', value: 'rgb(0, 0, 0);', default: true,
    }],
  });
