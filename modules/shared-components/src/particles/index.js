import React from 'react';
import Particles from 'react-particles-js';

// eslint-disable-next-line no-empty-pattern
const particles = ({
}) => (
  <Particles
    params={{
      particles: {
        number: {
          value: 600,
        },
        density: {
          enable: false,
        },
        line_linked: {
          color: '#000',
          distance: 30,
          opacity: 0.4,
        },
        move: {
          speed: 1,
        },
        modes: {
          bubble: {
            size: 6,
            distance: 40,
          },
        },
      },
    }}
    style={{
      width: '100%',
      position: 'fixed',
    }}
  />
);

particles.propTypes = {
};

export default particles;
