import React from 'react';
import PropTypes from 'prop-types';
// import { DFPManager } from 'react-dfp';
import ImageGallery from 'react-image-gallery';
import Particles from '../particles';
import { BaseGallery, ContentGallery } from './styled';
import { trackGallery } from '../../utils/analytics';
import 'react-image-gallery/styles/css/image-gallery.css';

const SliderGallery = ({
  sliderContent, backtheme, title,
}) => {
  if (global.window.screen.width > 768) {
    trackGallery('DW - Initiate - 1', title);
  }

  return (
    <BaseGallery backtheme={backtheme}>
      <Particles />
      <ContentGallery className="gallery">
        <ImageGallery
          items={sliderContent}
          title={title}
          type="gallery"
          showPlayButton={false}
        />
      </ContentGallery>
    </BaseGallery>
  );
};

SliderGallery.propTypes = {
  sliderContent: PropTypes.arrayOf(PropTypes.shape({
    original: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
  })).isRequired,
  backtheme: PropTypes.string,
  track: PropTypes.shape({
    component: PropTypes.string,
  }),
  title: PropTypes.string,
};

SliderGallery.defaultProps = {
  backtheme: 'gallery',
  track: {},
  title: '',
};

export default SliderGallery;
