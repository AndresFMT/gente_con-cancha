import { Box, Flex, Root } from 'rebass';
import styled from 'styled-components';
import get from 'lodash/get';

export const BaseGallery = styled(Box)`
  width: 100%;
  background: #000;
  height: 100vh;
  padding: 150px;
`;

export const GalleryGrid = styled(Flex)`
  ${Root} {
    margin-bottom: ${props => get(props, 'theme.space[4]', '')}px;
  }
`;

export const ContentGallery = styled(Box)`
  max-width: 1400px;
  margin: 0 auto;
  &.gallery {
    .image-gallery-image {
      display: flex;
      justify-content: center;
    }
    .image-gallery-description {
      width: 100%;
      max-width: 920px;
      left: initial;
      bottom: 10px;
      padding: 15px;
      line-height: 18px;
      font-size: 12px;
      &:before {
        content: 'Foto:';
        color: #e83338;
        font-style: italic;
        display: inline-block;
        margin-right: 5px;
      }
    }

    .image-gallery-fullscreen-button {
      top: 12px;
      right: 12px;
      width: 130px;
      height: 30px;
      background-color: rgba(0, 0, 0, 0.7);
      display: inline-flex;
      flex-direction: row-reverse;
      align-items: center;
      justify-content: space-between;

      &:before {
        content: '';
        background-image: url(${props => get(props, 'theme.icons.resizefullscreen', '')});
        background-size: 100%;
        padding: 0;
        display: block;
        height: 16px;
        width: 16px;
        transition: all ease-in-out .3s
      }

      &:hover {
        &:before {
          transform: rotate(90deg) scale(1.2);
        }
      }
      &:after {
        content: "AMPLIAR FOTO";
        display: block;
        font-size: 12px;
        font-weight: bold;
        color: #fff;
      }
    }
    .image-gallery-left-nav {
      left: -55px;
    }

    .image-gallery-right-nav {
      right: -55px;
    }
    .image-gallery-thumbnail {
      width: 25%;
      &.active {
        border: solid 2px #e83338;
      }
    }
  }
`;
