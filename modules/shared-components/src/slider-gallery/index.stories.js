import React from 'react';
import { storiesOf } from '@storybook/react';
// import { withKnobs } from '@storybook/addon-knobs';

import Gallery from '.';

const images = [
  {
    original: 'https://dev.gol.caracoltv.com/sites/default/files/100x500roja_1.png',
    thumbnail: 'https://dev.gol.caracoltv.com/sites/default/files/100x500roja_1.png',
    alt: 'Texto alternativo',
    description: 'Título',
  },
  {
    original: 'https://dev.gol.caracoltv.com/sites/default/files/100x500roja_0.png',
    thumbnail: 'https://dev.gol.caracoltv.com/sites/default/files/100x500roja_1.png',
    alt: 'dwwdwd wdwdfw Pie de foto',
    description: 'Phasellus nec sem in justo pellentesque facilisis. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed a libero. Phasellus viverra nulla ut metus varius laoreet. Donec posuere vulputate arcu.',
  },
  {
    original: 'https://dev.gol.caracoltv.com/sites/default/files/100x500roja_1.png',
    thumbnail: 'https://dev.gol.caracoltv.com/sites/default/files/100x500roja_1.png',
    alt: '',
    description: 'Phasellus nec sem in justo pellentesque facilisis. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed a libero. Phasellus viverra nulla ut metus varius laoreet. Donec posuere vulputate arcu.',
  },
  {
    original: 'https://dev.gol.caracoltv.com/sites/default/files/100x500roja_1.png',
    thumbnail: 'https://dev.gol.caracoltv.com/sites/default/files/100x500roja_1.png',
    alt: '',
    description: 'REGISTRADO',
  },
  {
    original: 'https://dev.gol.caracoltv.com/sites/default/files/100x500roja_1.png',
    thumbnail: 'https://dev.gol.caracoltv.com/sites/default/files/100x500roja_1.png',
    alt: '',
    description: '',
  },
  {
    original: 'https://dev.gol.caracoltv.com/sites/default/files/100x500roja_2.png',
    thumbnail: 'https://dev.gol.caracoltv.com/sites/default/files/100x500roja_1.png',
    alt: '',
    description: '',
  },
  {
    original: 'https://dev.gol.caracoltv.com/sites/default/files/1000x500negra_4.png',
    thumbnail: 'https://dev.gol.caracoltv.com/sites/default/files/1000x500negra_4.png',
    alt: '',
    description: '',
  },
];

storiesOf('Gallery', module)
  .add('with Gallery', () => <Gallery sliderContent={images} />);
