import { LazyLoadImage } from 'react-lazy-load-image-component';
import styled from 'styled-components';
import { Flex, Box, Heading } from 'rebass';
import get from 'lodash/get';

const imgStyle = {
  display: 'block',
  width: '100%',
};

export const BoxSlider = styled(Flex)`
  flex-direction: "row";
  max-width: ${props => get(props, 'theme.breakpoints.3', '')};
  margin: 0 auto;
  font-family: ${props => get(props, 'theme.fonts.sans', '')};
  position: relative;
  flex-wrap: wrap;
`;

export const HeaderImage = styled(LazyLoadImage)`
  ${imgStyle};
`;

export const ContentWrapper = styled(Flex)`
  padding: 0;
  display: flex;
  flex-direction: column;
  width: 49%;
  flex-wrap: wrap;
  padding-right: 10px;
`;

export const ImageWrapper = styled.div`
  position: relative;
  display: flex;
  width: 49%;
  padding-left: 10px;
`;

export const Title = styled(Heading)`
  color: ${props => (props.backtheme === 'dark'
    ? get(props, 'theme.colors.strawberry', '')
    : get(props, 'theme.colors.strawberry', ''))};
  font-size: ${props => get(props, 'theme.fontSizes[10]', '')}px;
  margin: ${props => get(props, 'theme.space[1]', '')}px 0;
  text-decoration: none;
`;

export const Description = styled.p`
  color: ${props => (props.backtheme === 'dark'
    ? get(props, 'theme.colors.black', '')
    : get(props, 'theme.colors.white', ''))};
  display: ${props => (props.sponsor ? 'none' : 'block')};
  font-size: ${props => get(props, 'theme.fontSizes[2]', '')}px;
  line-height: 24px;
  margin: ${props => get(props, 'theme.space[0]', '')}px;
  text-decoration: none;
`;


export const BoxContent = styled(Box)`
  display: flex;
  width: 100%;
  font-size: ${props => `${get(props, 'theme.fontSizes[2]', '') + 2}px`};
  p::first-letter {
    color: #000;
    float: left;
    font-size: 5em;
    margin: 0 .2em 0 0;
  }
`;
