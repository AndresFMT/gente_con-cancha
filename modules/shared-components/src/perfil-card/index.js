import React from 'react';
import PropTypes from 'prop-types';
import renderHTML from 'react-render-html';
import { BoxBase } from '../../../core/src/utils/styled';
import {
  BoxSlider, ImageWrapper,
  HeaderImage, ContentWrapper,
  Title, BoxContent,
} from './styled';

const PerfilCard = ({
  card,
}) => (
  <BoxBase>
    <BoxSlider>
      <ContentWrapper>
        <Title>
          {card.title}
        </Title>
        <BoxContent>
          {renderHTML(card.body)}
        </BoxContent>
      </ContentWrapper>
      <ImageWrapper>
        <HeaderImage effect="opacity" src={card.image.src} alt={card.image.src} />
      </ImageWrapper>
    </BoxSlider>
  </BoxBase>
);

PerfilCard.propTypes = {
  card: PropTypes.shape({
    body: PropTypes.string,
    title: PropTypes.string,
    image: PropTypes.shape({
      src: PropTypes.string,
      alt: PropTypes.string,
      title: PropTypes.string,
    }),
  }).isRequired,
};

PerfilCard.defaultProps = {
};

export default PerfilCard;
