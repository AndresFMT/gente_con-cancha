import React from 'react';
import { storiesOf } from '@storybook/react';

import PerfilCard from '.';

const cardPerfil = {
  body: '<p data-pm-slice="1 1 []"><strong>Luis Salcedo es bogotano, tiene 42, años, comenzó a jugar microfútbol desde los 8 años en un sector popular de Bogotá, en el barrio Kennedy. Y desde entonces, el micro ha sido todo en su vida. Así jugó en selecciones Colombia, con la que jugó Copas América en Brasil y Uruguay, Gran Prix en Brasil; jugó profesionalmente en Italia cuatro temporadas y después se fue a China, en donde primero fue jugador y después técnico. Ahora creó un club de micro en su barrio y allí transmite esos conocimientos adquiridos en su carrera. Cuenta con 80 niños entre los 4 y 16 años y se ha convertido en un líder deportivo de su barrio y comunidad.</strong></p> ',
  title: 'Clone of Luis Salcedo',
  image: {
    src: 'https://dev.gol.caracoltv.com/sites/default/files/perfil-1.jpg',
    alt: '',
    title: '',
  },
};

storiesOf('Perfil Card', module)
  .add('with Image', () => (
    <PerfilCard card={cardPerfil} backtheme="dark" />
  ), {
    backgrounds: [{
      name: 'Dark', value: '', default: true,
    }],
  });
