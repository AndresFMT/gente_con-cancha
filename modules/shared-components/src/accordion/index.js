import React from 'react';
import PropTypes from 'prop-types';
import { Box } from 'rebass';

import AccordionItem from './accordion-item';

const Accordion = ({ items }) => (
  <Box>
    {items.map(item => (
      <AccordionItem key={item.id} title={item.title} content={item.content} />
    ))}
  </Box>
);

Accordion.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.oneOfType([PropTypes.element, PropTypes.string]).isRequired,
    content: PropTypes.element.isRequired,
  })).isRequired,
};

export default Accordion;
