import React from 'react';
import { storiesOf } from '@storybook/react';

import Accordion from '.';

const items = [
  {
    id: 'ai1',
    title: 'Titulo 1',
    content: (
      <div>To using ook like readable English. Many desktop publishing packages</div>
    ),
  },
  {
    id: 'ai2',
    title: 'Titulo 2',
    content: (
      <div>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem</div>
    ),
  },
];

const items2 = [
  {
    id: 'ai1',
    title: 'Titulo M 1',
    content: <Accordion items={items} />,
  },
  {
    id: 'ai2',
    title: 'Titulo M 2',
    content: <Accordion items={items} />,
  },
];

storiesOf('Accordion', module)
  .add('Simple', () => (
    <Accordion items={items} />
  ), {
    backgrounds: [{
      name: 'Dark', value: 'rgba(0,14,38)', default: true,
    }],
  })
  .add('Multiple', () => (
    <Accordion items={items2} />
  ), {
    backgrounds: [{
      name: 'Dark', value: 'rgba(0,14,38)', default: true,
    }],
  });
