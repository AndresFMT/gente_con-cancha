import React, { useState } from 'react';
import PropTypes from 'prop-types';

import {
  BoxAccordionItem, TabTitle, BoxAccordionItemContent,
  BoxAccordionItemIcon,
} from './styled';

const AccordionItem = ({ title, content }) => {
  const [opened, setOpened] = useState(false);

  return (
    <BoxAccordionItem>
      <TabTitle
        onClick={() => setOpened(!opened)}
      >
        {title}
        <BoxAccordionItemIcon className={opened && 'opened-icon'} />
      </TabTitle>
      <BoxAccordionItemContent className={opened && 'opened'}>
        {opened && content}
      </BoxAccordionItemContent>
    </BoxAccordionItem>
  );
};

AccordionItem.propTypes = {
  title: PropTypes.oneOfType([PropTypes.element, PropTypes.string]).isRequired,
  content: PropTypes.element.isRequired,
};

export default AccordionItem;
