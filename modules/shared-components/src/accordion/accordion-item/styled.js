import styled from 'styled-components';
import { Box, Button } from 'rebass';
import get from 'lodash/get';

export const BoxAccordionItem = styled(Box)`
  width: 100%;
  margin-bottom: ${props => get(props, 'theme.space[2]', '')}px;
  &:last-child {
    margin-bottom: 0;
  }
`;
export const BoxAccordionItemIcon = styled.span`
  float: right;
  height: 24px;
  width: 24px;
  transition: transform 0.5s;
  background-size: contain;
  background-repeat: no-repeat;
  background-image: url(${props => get(props, 'theme.icons.arrowWhite', '')});
  background-position-y: 50%;
  &.opened-icon {
    transform: rotate(180deg);
  }
`;
export const TabTitle = styled(Button)`
  background-color: ${props => get(props, 'theme.colors.tdBackground1', '')};
  cursor: pointer;
  width: 100%;
  text-align: left;
  font-size: ${props => get(props, 'theme.fontSizes[3]', '')}px;
`;
export const BoxAccordionItemContent = styled(Box)`
  height: 0;
  marging: 10px;
  transition: .5s;
  width: calc(100% - 30px);
  margin-left: auto;
  color: ${props => get(props, 'theme.colors.white', '')};
  ${TabTitle} {
    font-size: ${props => get(props, 'theme.fontSizes[2]', '')}px;
  }
  &.opened {
    height: auto;
    padding: ${props => get(props, 'theme.space[2]', '')}px 0;
  }
`;
