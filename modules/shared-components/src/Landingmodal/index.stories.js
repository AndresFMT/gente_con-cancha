import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';

import Landingmodal from '.';

const items = {
  nid: '2002344',
  title: 'Gente con cancaha capitulo 445',
  autor: null,
  lead: 'https://dev.gol.caracoltv.com/sites/default/files/videoteaser6segundo.mp4',
  body: '<p><strong>José Sánchez</strong> se enamoró del arbitraje desde sus tiempos de adolescencia y desde entonces y hasta el sol de hoy, no ha podido dejar el pito, ni las tarjetas. A la par de cumplir labores de transporte de pacientes en recuperación que atienden las EPS, <strong>José</strong> saca tiempo para prepararse y estar cada sábado o domingo en las canchas para arbitrar los partidos de torneos aficionados sin importar su edad, la remuneración y las dificultades propias del que imparte justicia en el fútbol.</p> <p>',
  image: {
    src: 'https://dev.gol.caracoltv.com/sites/default/files/styles/is_type_1/public/portadamessi.jpg',
    alt: '',
    title: '',
  },
  video_youtube: 'https://www.youtube.com/watch?v=kI4dUyuvPTk',
  MediaStream: null,
  images: null,
};

storiesOf('Landing modal', module)
  .addDecorator(withKnobs)
  .add('default', () => (
    <Landingmodal itemLanding={items} />
  ));
