import React, { useState } from 'react';
import ReactPlayer from 'react-player';
import Modal from 'react-responsive-modal';
import ReactSVG from 'react-svg';
import PropTypes from 'prop-types';
// import renderHTML from 'react-render-html';
import ChapterBoxes from '../chapterBoxes';
// import ChapterFigure from '../chapterFigure';
import CloseModal from '../../icons/closeModal.svg';
import { BoxBase } from '../../../core/src/utils/styled';

import {
  ImageIcon, BoxIcon,
  BoxVideo, PlayerContent,
} from './styled';

const Landingmodal = ({ itemLanding }) => {
  const [event, setEvent] = useState(false);
  const [clicked, setClicked] = useState(true);
  const [clickOpen, setclickOpen] = useState(false);
  // const [open, setOpen] = useState('closed');
  const hide = () => {
    setClicked(!clicked);
    setEvent(false);
  };
  return (
    <BoxBase flexWrap="wrap" id="modalcontent">
      <Modal
        classNames={{
          overlay: 'modal-overlay',
          modal: 'modal',
          closeButton: 'modal-close-button',
        }}
        open={event}
        onClose={hide}
        container={document.getElementById('modalcontent')}
        showCloseIcon={false}
        focusTrapped={false}
        blockScroll={false}
        styles={{
          modal: {
            maxWidth: '1355px',
            width: '100%',
            padding: 'unset',
          },
          overlay: {
            background: 'rgba(0, 0, 0, 0.5)',
            position: 'fixed',
          },
        }}
        animationDuration={1000}
        center
      >
        <BoxVideo width={1}>
          <PlayerContent>
            <ReactPlayer
              className="react-player"
              width="100%"
              height="100%"
              url={clicked ? itemLanding.video_youtube : ''}
              // url="https://www.youtube.com/watch?v=kI4dUyuvPTk"
              // eslint-disable-next-line react/jsx-boolean-value
              playing={event}
            />
            <ImageIcon
              onClick={() => {
                // eslint-disable-next-line no-unused-expressions
                setClicked(!clicked);
                setEvent(false);
                // setOpen('closed');
                setclickOpen(!clickOpen);
              }}
            >
              <ReactSVG src={CloseModal} />
            </ImageIcon>
          </PlayerContent>
        </BoxVideo>
      </Modal>
      <BoxIcon
        width={1}
        onClick={() => {
          setEvent(true);
          setClicked(true);
          // setOpen('closed');
          setclickOpen(!clickOpen);
        }}
      >
        <ChapterBoxes
          item={itemLanding}
        />
      </BoxIcon>
      {/* <ShowFigure className={clickOpen ? 'closed' : 'opened'}>
        <ChapterFigure
          // eslint-disable-next-line react/no-array-index-key
          initialState={open}
          item={itemLanding}
        />
      </ShowFigure> */}
    </BoxBase>
  );
};

Landingmodal.propTypes = {
  itemLanding: PropTypes.shape({
    nid: PropTypes.string,
    title: PropTypes.string,
    autor: null,
    body: PropTypes.string,
    image: PropTypes.shape({
      src: PropTypes.string,
      alt: PropTypes.string,
      title: PropTypes.string,
    }),
    episode: PropTypes.string,
    lead: PropTypes.string,
    videoType: PropTypes.string,
    video_youtube: PropTypes.string,
    MediaStream: null,
    images: null,
  }).isRequired,
};

Landingmodal.defaultProps = {
};

export default Landingmodal;
