import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import { Box } from 'rebass';
import Share from '.';


storiesOf('Share', module)
  .addDecorator(withKnobs)
  .add('Desktop', () => (
    <Box>
      <Share url="genteconcancha.com" mobile />
    </Box>
  ), {
    backgrounds: [{
      name: 'black', value: 'rgba(0,14,38)', default: true,
    }],
  })
  .add('Desktop Light', () => (
    <Box>
      <Share backTheme="light" url="genteconcancha.com" mobile />
    </Box>
  ), {
    backgrounds: [],
  })
  .add('Desktop Light Column', () => (
    <Box>
      <Share backTheme="light" url="genteconcancha.com" shareDirection="column" mobile />
    </Box>
  ), {
    backgrounds: [],
  });
