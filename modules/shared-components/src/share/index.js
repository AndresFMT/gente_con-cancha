/* eslint-disable no-sequences */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import ShareItem from './share-item';

import Facebook from '../../icons/facebookCancha.svg';
import Twitter from '../../icons/twitterCancha.svg';
import Whatsapp from '../../icons/whatsappCancha.svg';


import {
  Root,
  Title,
} from './styled';

const Share = ({
  shareList,
  title,
  mobile,
  backTheme,
  shareDirection,
  shareUrl,
  shareDescription,
}) => {
  const [width, setWidth] = useState(window.innerWidth);
  const componentUrl = shareUrl ? window.location.origin + shareUrl : null;
  const url = componentUrl || window.location;
  const titleSplit = document.title.split('|');
  const genteTitle = titleSplit.length > 1 ? titleSplit[1] : titleSplit[0];
  const compDescription = shareDescription ? `${shareDescription} | ${genteTitle}` : null;
  const urlTitle = compDescription || document.title;

  const shareListDefault = [
    {
      id: 'Facebook',
      icon: Facebook,
      background: props => get(props, 'theme.colors.facebook', ''),
      title: 'Comparte este artículo',
      shareUrl: `https://www.facebook.com/sharer/sharer.php?u=${url}`,
    },
    {
      id: 'Twitter',
      icon: Twitter,
      background: props => get(props, 'theme.colors.twitter', ''),
      shareUrl: `https://twitter.com/intent/tweet?text=${urlTitle.replace('|', '')}&url=${url}`,
    },
    {
      id: 'Whatsapp',
      icon: Whatsapp,
      background: props => get(props, 'theme.colors.whatsApp', ''),
      shareUrl: `https://api.whatsapp.com/send?text=${urlTitle} ${url}`,
    },
  ];
  const shareListComp = !isEmpty(shareList) ? shareList : shareListDefault;

  useEffect(() => {
    const handleResize = () => setWidth(window.innerWidth);
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  });

  return (
    <Root alignItems="center" flexDirection={shareDirection}>
      {title && (
        <Title
          backTheme={backTheme}
          direction={shareDirection}
          mobile={width < 768 ? mobile : false}
        >
          {title}
        </Title>
      )}
      {shareListComp.map((buttonInfo, key) => (
        <ShareItem
          {...buttonInfo}
          key={buttonInfo.id || key}
          backTheme={backTheme}
          direction={shareDirection}
          mobile={width < 768 ? mobile : false}
          url={`${buttonInfo.shareUrl}`}
          onClick={() => ('Content', `DW - Share ${document.title}`, `Share in: ${buttonInfo.id}`)}
        />
      ))}
    </Root>
  );
};

Share.propTypes = {
  title: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  mobile: PropTypes.bool,
  backTheme: PropTypes.string,
  shareList: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  shareDirection: PropTypes.string,
  shareUrl: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  shareDescription: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
};

Share.defaultProps = {
  title: 'Comparte este artículo',
  backTheme: 'light',
  shareDirection: 'row',
  mobile: true,
  shareList: [],
  shareUrl: null,
  shareDescription: null,
};

export default Share;
