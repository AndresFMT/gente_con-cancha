import { Flex, Link, Box } from 'rebass';
import styled from 'styled-components';
import get from 'lodash/get';

export const Root = styled(Flex)`
  font-family: ${props => get(props, 'theme.fonts.sans', '')};
  text-align: center;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 8;
  ${(props) => {
    if (props.flexDirection === 'column' && !props.mobile) {
      return 'position: relative';
    } if (props.mobile) {
      return 'position: fixed';
    }
    return '';
  }};
`;

export const Title = styled(Flex)`
  align-items: center;
  font-size: ${props => get(props, 'theme.fontSizes[2]', '')}px;
  justify-content: center;
  height: ${props => (props.mobile === true ? '60px' : 'auto')};
  color: ${props => get(props, 'theme.colors.marineBlue', '')};
  font-weight: bold;
  margin-right: 10px;

`;

export const LinkItem = styled(Link)`
  align-items: center;
  display: flex;
  justify-content: center;
  height: ${props => (props.mobile === true ? '30px' : '35px')};
  margin: 0 15px;
`;

export const ImageIcon = styled(Box)`
  height: ${props => (props.mobile === true ? '30px' : '36px')};
  width: ${props => (props.mobile === true ? '30px' : '36px')};
  margin: 0;
  svg {
    height: ${props => (props.mobile === true ? '30px' : '36px')};
    width: ${props => (props.mobile === true ? '30px' : '36px')};

      path {
      transition: fill 0.5s ease-in-out;
      fill: ${(props) => {
    if (props.colorIcon === 'Facebook' && props.backTheme === 'light') {
      return get(props, 'theme.colors.strawberry', '');
    } if (props.colorIcon === 'Twitter' && props.backTheme === 'light') {
      return get(props, 'theme.colors.strawberry', '');
    } if (props.colorIcon === 'Whatsapp' && props.backTheme === 'light') {
      return get(props, 'theme.colors.strawberry', '');
    }
  }};
    };
    &:hover {
      path {
      fill: ${(props) => {
    if (props.colorIcon === 'Facebook' && props.backTheme === 'light') {
      return get(props, 'theme.colors.marineBlue', '');
    } if (props.colorIcon === 'Twitter' && props.backTheme === 'light') {
      return get(props, 'theme.colors.marineBlue', '');
    } if (props.colorIcon === 'Whatsapp' && props.backTheme === 'light') {
      return get(props, 'theme.colors.marineBlue', '');
    }
  }};
        };
      }
    }
    /* @media (min-width: ${props => get(props, 'theme.breakpoints.2', '')}) {
      display: ${(props) => {
    if (props.colorIcon === 'Whatsapp') {
      return 'none';
    }
    return '';
  }};
    } */
`;
