/* eslint-disable no-sequences */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Image } from 'rebass';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import ShareItem from './share-item';

import Facebook from '../../icons/facebookCancha.svg';
import Twitter from '../../icons/twitterCancha.svg';
import Whatsapp from '../../icons/whatsappCancha.svg';
import ShareIcon from '../../icons/share.svg';
import IconClose from '../../icons/close.svg';

import {
  Root, Button, ContentFlex,
} from './styled';

const ShareGenteConCancha = ({
  shareList, backTheme, shareUrl, shareDescription, shareDirection,
}) => {
  const componentUrl = shareUrl ? window.location.origin + shareUrl : null;
  const url = componentUrl || window.location;
  const titleSplit = document.title.split('|');
  const genteTitle = titleSplit.length > 1 ? titleSplit[1] : titleSplit[0];
  const compDescription = shareDescription ? `${shareDescription} | ${genteTitle}` : null;
  const urlTitle = compDescription || document.title;
  const [event, setEvent] = useState('closed');
  const [share, setShare] = useState(false);
  const shareListDefault = [
    {
      id: 'Facebook',
      icon: Facebook,
      background: props => get(props, 'theme.colors.facebook', ''),
      shareUrl: `https://www.facebook.com/sharer/sharer.php?u=${url}`,
    },
    {
      id: 'Twitter',
      icon: Twitter,
      background: props => get(props, 'theme.colors.twitter', ''),
      shareUrl: `https://twitter.com/intent/tweet?text=${urlTitle.replace('|', '')}&url=${url}`,
    },
    {
      id: 'Whatsapp',
      icon: Whatsapp,
      background: props => get(props, 'theme.colors.whatsApp', ''),
      shareUrl: `https://api.whatsapp.com/send?text=${urlTitle} ${url}`,
    },
  ];
  const shareListComp = !isEmpty(shareList) ? shareList : shareListDefault;

  return (
    <ContentFlex className={share ? 'opened' : 'closed'}>
      <Button onClick={() => {
        setShare(!share);
        setEvent('opened');
      }}
      >
        <Image src={share ? IconClose : ShareIcon} />
      </Button>
      <Root
        alignItems="center"
        flexDirection={shareDirection}
      >
        {shareListComp.map((buttonInfo, key) => (
          <ShareItem
            initialState={event}
            {...buttonInfo}
            key={buttonInfo.id || key}
            backTheme={backTheme}
            url={`${buttonInfo.shareUrl}`}
            direction={shareDirection}
            onClick={() => ('Content', `DW - Share ${document.title}`, `Share in: ${buttonInfo.id}`)}
          />
        ))}
      </Root>
    </ContentFlex>
  );
};

ShareGenteConCancha.propTypes = {
  backTheme: PropTypes.string,
  shareList: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  shareDirection: PropTypes.string,
  shareUrl: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  shareDescription: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
};

ShareGenteConCancha.defaultProps = {
  backTheme: 'light',
  shareDirection: 'row',
  shareList: [],
  shareUrl: null,
  shareDescription: null,
};

export default ShareGenteConCancha;
