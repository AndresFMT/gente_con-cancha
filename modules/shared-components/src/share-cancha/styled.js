import styled from 'styled-components';
import {
  Image, Flex, Link, Box, Button as ButtonBase,
} from 'rebass';

import get from 'lodash/get';

export const Button = styled(ButtonBase)`
  padding: 0;
  background-color: transparent;
  margin-bottom: 12px;
  cursor: pointer;
  &:focus {
    outline: 0;
  }
`;

export const Root = styled(Flex)`
  font-family: ${props => get(props, 'theme.fonts.sans', '')};
  text-align: center;
  flex-direction: column;
  left: 0;
  right: 0;
  bottom: 0;
  transition: opacity 0.8s ease-out;
  opacity: 0;
  height: 0;
  z-index: -1;
  position: absolute;
  top: 50px;
  ${(props) => {
    if (props.flexDirection === 'column') {
      return 'position: relative';
    }
    return '';
  }};
`;

export const ContentFlex = styled(Flex)`
  width: 36px;
  height: 57px;
  justify-content: center;
  position: relative;

  &.opened{
    ${Root} {
      opacity: 1;
      height: auto;
      z-index: 8;
    }
    ${Button} {
      ${Image} {
        transition: 0.6s ease-out;
        transform: rotate(90deg);
      }
    }
  }
  ${Button} {
    width: 36px;
    height: 36px;
    margin: 0;
    ${Image} {
      &:hover {
        transition: 0.8s ease-out;
        transform: rotate(40deg);
      }
    }
  }
`;


export const LinkItem = styled(Link)`
  align-items: center;
  background-color: transparent;
  display: flex;
  justify-content: center;
  flex-direction: column;
  width: 36px;
  height: 36px;
  margin: 0;
  margin-top: 5px;
  flex: ${props => (props.mobile === false ? '1' : 'inherit')};
`;

export const ShareItem = styled(Link)`
  margin: 0;
`;

export const ImageIcon = styled(Box)`
  height: 36px;
  width: 36px;
  margin: 0;
  svg {
    height: 36px;
    width: 36px;
      path {
      transition: fill 0.5s ease-in-out;
      /* transition-delay: 1s; */
      fill: ${(props) => {
    if (props.colorIcon === 'Facebook' && props.backTheme === 'light') {
      return get(props, 'theme.colors.strawberry', '');
    } if (props.colorIcon === 'Twitter' && props.backTheme === 'light') {
      return get(props, 'theme.colors.strawberry', '');
    } if (props.colorIcon === 'Whatsapp' && props.backTheme === 'light') {
      return get(props, 'theme.colors.strawberry', '');
    }
  }};
    };
    &:hover {
      path {
      fill: ${(props) => {
    if (props.colorIcon === 'Facebook' && props.backTheme === 'light') {
      return get(props, 'theme.colors.marineBlue', '');
    } if (props.colorIcon === 'Twitter' && props.backTheme === 'light') {
      return get(props, 'theme.colors.marineBlue', '');
    } if (props.colorIcon === 'Whatsapp' && props.backTheme === 'light') {
      return get(props, 'theme.colors.marineBlue', '');
    }
  }};
        };
      }
  }
`;
