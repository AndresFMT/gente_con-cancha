import React from 'react';
import PropTypes from 'prop-types';
import ReactSVG from 'react-svg';
import {
  LinkItem,
  ImageIcon,
} from './styled';

const ShareItem = ({
  icon,
  url,
  id,
  background,
  backTheme,
  mobile,
  direction,
  onClick,
}) => (
  <LinkItem
    alignItems="center"
    justifyContent="center"
    background={background}
    direction={direction}
    mobile={mobile}
    href={url}
    target={id !== 'Email' ? '_blank' : null}
    onClick={() => onClick()}
    m={2}
  >
    <ImageIcon colorIcon={id} backTheme={backTheme}>
      <ReactSVG className={backTheme} src={icon} />
    </ImageIcon>
  </LinkItem>
);

ShareItem.propTypes = {
  id: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
  mobile: PropTypes.bool,
  backTheme: PropTypes.string.isRequired,
  background: PropTypes.func.isRequired,
  direction: PropTypes.string.isRequired,
  onClick: PropTypes.func,
};

ShareItem.defaultProps = {
  mobile: false,
  onClick: () => {},
};

export default ShareItem;
