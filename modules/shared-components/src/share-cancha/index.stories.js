import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import { Box } from 'rebass';
import ShareGenteConCancha from '.';

storiesOf('ShareGenteConCancha', module)
  .addDecorator(withKnobs)
  .add('Share Hide', () => (
    <Box>
      <ShareGenteConCancha backTheme="light" url="cancha.com" shareDirection="column" />
    </Box>
  ), {
    background: [],
  })
  .add('Share Show', () => (
    <Box>
      <ShareGenteConCancha backTheme="light" url="genteconcancha.com" />
    </Box>
  ), {
    backgrounds: [],
  });
