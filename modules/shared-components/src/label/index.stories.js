import React from 'react';
import { storiesOf } from '@storybook/react';
import {
  withKnobs,
} from '@storybook/addon-knobs';
import Label from '.';

storiesOf('Label', module)
  .addDecorator(withKnobs)
  .add('Label', () => (
    <Label
      text="Episodio"
      duration="10:00"
    />
  ), {
    backgrounds: [{
      name: 'black', value: 'rgba(0,14,38)', default: true,
    }],
  });
