import { Box, Flex } from 'rebass';
import styled from 'styled-components';
import get from 'lodash/get';

export const LabelBox = styled(Flex)`
  background-color: ${props => `${get(props, 'theme.colors.white', '')}`};
  color: ${props => `${get(props, 'theme.colors.tdBackground1', '')}`};
  font-size: ${props => `${get(props, 'theme.fontSizes[0]', '')}px`};
  line-height: 16px;
  font-family: ${props => `${get(props, 'theme.fonts.sans', '')}`};
  flex-direction: row;
`;

export const LabelText = styled(Box)`
  background-color: ${props => `${get(props, 'theme.colors.discovery', '')}`};
  color: ${props => `${get(props, 'theme.colors.tdBackground1', '')}`};
  font-weight: bold;
  padding: 4px ${props => `${get(props, 'theme.space[1]', '')}px`};
  text-transform: uppercase;
    @media (min-width: ${props => get(props, 'theme.breakpoints.2', '768px')}) {
      padding: ${props => `${get(props, 'theme.space[1]', '')}px`} 
      ${props => `${get(props, 'theme.space[1]', '')}px`};
    }
`;

export const Duration = styled(Box)`
  background-color: ${props => `${get(props, 'theme.colors.white', '')}`};
  padding: 4px ${props => `${get(props, 'theme.space[1]', '')}px`};
    @media (min-width: ${props => get(props, 'theme.breakpoints.2', '768px')}) {
      padding: ${props => `${get(props, 'theme.space[1]', '')}px`} 
      ${props => `${get(props, 'theme.space[1]', '')}px`};
    }
`;
