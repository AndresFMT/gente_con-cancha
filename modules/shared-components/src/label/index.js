import React from 'react';
import PropTypes from 'prop-types';
import {
  LabelBox, LabelText, Duration,
} from './styled';

const Label = ({ text, duration }) => (
  <LabelBox>
    <LabelText>{text}</LabelText>
    <Duration>{duration}</Duration>
  </LabelBox>
);

Label.propTypes = {
  text: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
  duration: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
};

Label.defaultProps = {
  text: '',
  duration: '--:--',
};

export default Label;
