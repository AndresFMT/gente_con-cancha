/* eslint-disable react/button-has-type */
import React from 'react';
import PropTypes from 'prop-types';
import renderHTML from 'react-render-html';
import {
  Box, Image,
} from 'rebass';
import {
  BoxBase, HeadingText, CardImage, BoxBody,
} from './styled';
import patterChapterFigure from '../../../core/public/images/patterChapterFigure.png';

const ChapterFigure = ({ item }) => {
  const iconStyle = {
    backgroundImage: `url(${patterChapterFigure})`,
  };
  return (
    <>
      <Box width={1} flexWrap="wrap" style={iconStyle} p={50}>
        <BoxBase>
          <HeadingText>{item.title}</HeadingText>
          <CardImage>
            <Image src={item.image.src} />
          </CardImage>
          <BoxBody>
            {renderHTML(item.body)}
          </BoxBody>
        </BoxBase>
      </Box>
    </>
  );
};

ChapterFigure.propTypes = {
  item: PropTypes.shape({
    nid: PropTypes.string,
    title: PropTypes.string,
    autor: null,
    body: PropTypes.string,
    image: PropTypes.shape({
      src: PropTypes.string,
      alt: PropTypes.string,
      title: PropTypes.string,
    }),
    episode: PropTypes.string,
    lead: PropTypes.string,
    videoType: PropTypes.string,
    video_youtube: PropTypes.string,
    MediaStream: null,
    images: null,
  }).isRequired,
};

ChapterFigure.defaultProps = {
};

export default ChapterFigure;
