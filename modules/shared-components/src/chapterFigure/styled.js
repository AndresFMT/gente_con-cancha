import {
  Heading, Box, Card,
} from 'rebass';
import styled from 'styled-components';
import get from 'lodash/get';

export const BoxBase = styled(Box)`
  display: flex;
  justify-content: space-between;
  position: relative;
  max-width: ${props => get(props, 'theme.breakpoints.2', '1440px')};
  margin: 0 auto;
  width: 100%;
`;

// eslint-disable-next-line import/prefer-default-export
export const HeadingText = styled(Heading)`
  display: flex;
  position: absolute;
  top: 20px;
  left: 20%;
  justify-content: center;
  width: auto;
  background-color: ${props => get(props, 'theme.colors.white', '')};
  color: ${props => get(props, 'theme.colors.marineBlue', '')};
  font-size: ${props => get(props, 'theme.fontSizes[9]', '')}px;
  padding: 16px 32px;
`;

export const CardImage = styled(Card)`
  display: flex;
  width: 40%;
  height: 370px;
`;

export const BoxBody = styled(Box)`
  display: flex;
  width: 70%;
  padding: ${props => get(props, 'theme.space[3]', '')}px;
  padding-bottom: 0;
  align-items: flex-end;
  p {
    margin: 0;
    color: ${props => get(props, 'theme.colors.marineBlue', '')};
    font-size: ${props => get(props, 'theme.fontSizes[3]', '')}px;
    line-height: 30px;
  }
`;
