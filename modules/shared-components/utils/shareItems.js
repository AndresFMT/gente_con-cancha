import get from 'lodash/get';
import Facebook from '../icons/facebook.svg';
import Twitter from '../icons/twitter.svg';
import Email from '../icons/email.svg';
import Whatsapp from '../icons/whatsapp.svg';

export const shareData = [
  {
    id: 1,
    icon: Facebook,
    color: 'blue',
    url: '',
    background: props => get(props, 'theme.colors.facebook', ''),
    title: '',
  },
  {
    id: 2,
    icon: Twitter,
    color: 'bluetw',
    url: '',
    background: props => get(props, 'theme.colors.twitter', ''),
    title: '',
  },
  {
    id: 4,
    icon: Email,
    color: 'red',
    url: '',
    background: props => get(props, 'theme.colors.mail', ''),
    title: '',
  }];

export const shareMobile = [
  {
    id: 1,
    icon: Facebook,
    color: 'white',
    url: '',
    background: props => get(props, 'theme.colors.facebook', ''),
    title: '',
  },
  {
    id: 2,
    icon: Twitter,
    color: 'white',
    url: '',
    background: props => get(props, 'theme.colors.twitter', ''),
    title: '',
  },
  {
    id: 3,
    icon: Whatsapp,
    color: 'white',
    url: '',
    background: props => get(props, 'theme.colors.whatsApp', ''),
    title: '',
  },
  {
    id: 4,
    icon: Email,
    color: 'white',
    url: '',
    background: props => get(props, 'theme.colors.mail', ''),
    title: '',
  }];

export const urlList = [
  {
    id: 'Facebook',
    url: '/home',
  },
  {
    id: 'Twitter',
    url: '/home',
  },
  {
    id: 'Email',
    url: '/home',
  }];
