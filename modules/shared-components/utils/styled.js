import styled from 'styled-components';
import get from 'lodash/get';
import { Box } from 'rebass';

// eslint-disable-next-line import/prefer-default-export
export const Gradient = styled(Box)`
  background: linear-gradient(
  ${props => get(props, 'theme.colors.marineBlue', '')} 0%,
  ${props => get(props, 'theme.colors.marineBlue', '')} 40%,
  ${props => get(props, 'theme.colors.marineBlue', '')} 60%,
  ${props => get(props, 'theme.colors.marineBlue', '')} 100%);
`;

export const getFont = props => `font-family: ${get(props, 'theme.fonts.sans')};`;

export const getColor = color => props => `color: ${get(props, `theme.colors.${color}`)};`;
