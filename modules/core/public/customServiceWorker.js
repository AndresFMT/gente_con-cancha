/* eslint-disable */
importScripts(
  'https://storage.googleapis.com/workbox-cdn/releases/3.6.3/workbox-sw.js',
);

const FALLBACK_URL = '/offline';

if (workbox) {
  console.log('Yay! Workbox is loaded 🎉');
} else {
  console.log('Boo! Workbox didn\'t load 😬');
}

// TODO remove workbox to use a native service worker
workbox.routing.registerRoute(
  /\.(?:js|css|html|svg|png|jpg)$/,
  workbox.strategies.cacheFirst({
    cacheName: 'data-static',
    plugins: [
      new workbox.expiration.Plugin({
        maxAgeSeconds: 60 * 60 * 24 * 7,
        maxEntries: 50,
        purgeOnQuotaError: true,
      }),
    ],
  }),
);

// const urlHandler = workbox.strategies.staleWhileRevalidate({
//   cacheName: 'page-cache',
// });

// workbox.routing.registerRoute(
//   /\/.+\//,
//   ({event}) => {
//     return urlHandler.handle({event})
//       .then((response) => {
//         return caches.match(FALLBACK_URL);
//       })
//       .catch(() => caches.match(FALLBACK_URL));
// });

/* injection point for manifest files.  */
workbox.precaching.precacheAndRoute([]);
