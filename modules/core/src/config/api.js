import axios from 'axios';

const config = { baseURL: '/api' };

if (process.env.NODE_ENV === 'development') {
  config.baseURL = 'https://tomalo.herokuapp.com/https://dev.gol.caracoltv.com/api/gente-con-cancha';
  // https://dev.gol.caracoltv.com/api/cancha-menu
}

const api = axios.create(config);

export const setBaseUrlLang = (langcode) => {
  api.defaults.params = { langcode, t: parseInt(new Date().getTime() / 100000, 10) };
};

export default api;
