import TagManager from 'react-gtm-module';

const initialize = (langcode) => {
  const gtmId = langcode === 'pt-COL' ? 'GTM-T4HCRP' : 'GTM-T4HCRP';
  TagManager.initialize({ gtmId });
};

const GoogleTagManager = {
  initialize,
};

export default GoogleTagManager;
