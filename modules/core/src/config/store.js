/* eslint-disable no-underscore-dangle */
import { createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware } from 'connected-react-router';
import thunk from 'redux-thunk';
import { createBrowserHistory } from 'history';

// Reducers
import rootReducer from '../reducers';

export default (url = '/') => {
  const history = createBrowserHistory({
    basename: url,
  });

  const enhancers = [];

  // Dev tools are helpful
  if (process.env.NODE_ENV === 'development') {
    const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;


    if (typeof devToolsExtension === 'function') {
      enhancers.push(devToolsExtension());
    }
  }

  const middleware = [thunk, routerMiddleware(history)];
  const composedEnhancers = compose(
    applyMiddleware(...middleware),
    ...enhancers,
  );

  // Do we have preloaded state available? Great, save it.
  const initialState = window.__PRELOADED_STATE__;

  // Delete it once we have it stored in a variable
  delete window.__PRELOADED_STATE__;

  // Create the store
  const store = createStore(
    rootReducer(history),
    initialState,
    composedEnhancers,
  );
  return {
    store,
    history,
  };
};
