// Libraries
import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Loadable from 'react-loadable';
import { Loading as LoadingComponent } from '@cancha/shared-components';

// eslint-disable-next-line react/prop-types
const Loading = ({ error = null }) => {
  if (error) {
    // eslint-disable-next-line no-console
    console.log('error', error);
  }
  return <LoadingComponent />;
};

const Home = Loadable({
  loader: () => import(/* webpackChunkName: "home" */ '../pages/Home'),
  loading: Loading,
  modules: ['homepage'],
});

const Extras = Loadable({
  loader: () => import(/* webpackChunkName: "home" */ '../pages/Extras'),
  loading: Loading,
  modules: ['extras'],
});

const Perfiles = Loadable({
  loader: () => import(/* webpackChunkName: "home" */ '../pages/Perfiles'),
  loading: Loading,
  modules: ['perfiles'],
});

const Galerias = Loadable({
  loader: () => import(/* webpackChunkName: "home" */ '../pages/Galerias'),
  loading: Loading,
  modules: ['galerias'],
});

const Test = Loadable({
  loader: () => import(/* webpackChunkName: "test" */ '../pages/Test'),
  loading: Loading,
  modules: ['test'],
});

const Routes = () => (
  <Switch>
    <Route exact path="/capitulo" component={Home} />
    <Route exact path="/galerias" component={Galerias} />
    <Route exact path="/perfiles" component={Perfiles} />
    <Route exact path="/extras" component={Extras} />
    <Route path="/test" component={Test} />
    <Route path="/" exact component={Home} />
  </Switch>
);

export default Routes;
