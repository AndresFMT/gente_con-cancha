import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import Helmet from 'react-helmet';
import { injectIntl, intlShape } from 'react-intl';
import { CanchaMenu } from '@cancha/shared-components';
// import logo from '../../../public/images/logo-compuesto.svg';
import StyledPage from './styled';

const defaultSep = ' | ';

class Page extends Component {
  static getMetaTags(
    props,
    pathname,
  ) {
    const {
      title, description, image, contentType, noCrawl, intl, keywords, titleSecond,
    } = props;
    const defaultDescription = intl.formatMessage({
      id: 'Page.Description',
      defaultMessage: '',
    });
    const defaultTitle = intl.formatMessage({
      id: 'Page.Title',
      defaultMessage: 'gente',
    });
    let title2;
    if (titleSecond) {
      title2 = titleSecond;
    } else {
      title2 = defaultTitle;
    }
    const SITE_URL = process.env.NODE_ENV === 'development'
      ? 'http://localhost:3000'
      : intl.formatMessage({
        id: 'Page.Url',
        defaultMessage: 'https://www.genteconcancha.com',
      });
    // TODO add default logo
    const defaultImage = `${SITE_URL}`;
    const defaultTwitter = '@genteconcancha';

    const metaTitle = title
      ? (title + defaultSep + title2).substring(0, 60)
      : defaultTitle;
    const metaDescription = description
      ? description.substring(0, 155)
      : defaultDescription;
    const metaImage = image || defaultImage;

    const metaTags = [
      { itemprop: 'name', content: metaTitle },
      { itemprop: 'description', content: metaDescription },
      { itemprop: 'image', content: metaImage },
      { name: 'description', content: metaDescription },
      { name: 'keywords', content: keywords },
      { name: 'twitter:card', content: 'summary_large_image' },
      { name: 'twitter:site', content: defaultTwitter },
      { name: 'twitter:title', content: metaTitle },
      { name: 'twitter:description', content: metaDescription },
      { name: 'twitter:creator', content: defaultTwitter },
      { name: 'twitter:image:src', content: metaImage },
      { property: 'og:title', content: metaTitle },
      { property: 'og:type', content: contentType || 'website' },
      { property: 'og:url', content: SITE_URL + pathname },
      { property: 'og:image', content: metaImage },
      { property: 'og:description', content: metaDescription },
      { property: 'og:site_name', content: defaultTitle },
    ];

    if (noCrawl) {
      metaTags.push({ name: 'robots', content: 'noindex, nofollow' });
    }

    return metaTags;
  }

  componentDidMount() {
    // eslint-disable-next-line react/prop-types
  }

  render() {
    const {
      children, id, className, type, intl, menuList, menuTheme,
    } = this.props;

    const lang = intl.locale.split('-')[0];
    const { pathname } = window.location;
    const { title, titleSecond } = this.props;

    let defaultTitle;
    if (titleSecond) {
      defaultTitle = titleSecond;
    } else {
      defaultTitle = intl.formatMessage({
        id: 'Page.Title',
        defaultMessage: 'genteconcancha',
      });
    }
    const SITE_URL = process.env.NODE_ENV === 'development'
      ? 'http://localhost:3000'
      : intl.formatMessage({
        id: 'Page.Url',
        defaultMessage: 'https://www.genteconcancha.com',
      });

    return (
      <StyledPage id={id} className={className} type={type} flexDirection="column">
        <CanchaMenu
          order={0}
          menuList={menuList}
          menuTheme={menuTheme}
          activeClassName="active"
        />
        <Helmet
          htmlAttributes={{
            lang,
            itemscope: undefined,
            itemtype: 'http://schema.org/WebPage',
          }}
          title={
            title ? title + defaultSep + defaultTitle : defaultTitle
          }
          link={[
            {
              rel: 'canonical',
              href: SITE_URL + pathname,
            },
          ]}
          meta={Page.getMetaTags(this.props, pathname)}
        />
        {children}
      </StyledPage>
    );
  }
}

Page.propTypes = {
  id: PropTypes.string.isRequired,
  className: PropTypes.string,
  location: PropTypes.shape({
    pathname: PropTypes.string,
  }).isRequired,
  children: PropTypes.node.isRequired,
  // eslint-disable-next-line react/require-default-props
  type: PropTypes.string,
  intl: intlShape.isRequired,
  title: PropTypes.string,
  titleSecond: PropTypes.string,
  history: PropTypes.shape({}).isRequired,
  menuList: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    shortname: PropTypes.string.isRequired,
  })),
  menuTheme: PropTypes.string,
};

Page.defaultProps = {
  className: '',
  menuList: [],
  title: '',
  titleSecond: null,
  menuTheme: 'default',
};

export default injectIntl(withRouter(Page));
