import styled, { createGlobalStyle } from 'styled-components';
import { Flex } from 'rebass';
import get from 'lodash/get';

const StyledPage = styled(Flex)`


${(props) => {
    global.console.log(props);
    if (props.id === 'galerias' || props.id === 'capitulo' || props.id === 'extras') {
      return `
      .header {
        position: absolute;
      }
      `;
    }
    if (props.type === 'gray') {
      return `background-color: ${get(props, 'theme.colors.tdBlueGray', '')};`;
    }
    if (props.type === 'light') {
      return `background-color: ${get(props, 'theme.colors.white', '')};`;
    }

    if (props.type === 'gradient') {
      return `background-color: ${get(props, 'theme.colors.marineBlue', '')};`;
    }
    return `background-color: ${get(props, 'theme.colors.marineBlue', '')};`;
  }
};
`;

export default StyledPage;
