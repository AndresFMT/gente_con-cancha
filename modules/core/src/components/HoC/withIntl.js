// Libraries
import React from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { IntlProvider, addLocaleData } from 'react-intl';

// Locales
// import pt from 'react-intl/locale-data/pt';
import es from 'react-intl/locale-data/es';
// import en from 'react-intl/locale-data/en';
// Helpers
import { setBaseUrlLang } from '../../config/api';

// Action
import { appSetLangAction } from '../../actions/app';

// Api
// import { setIPBR } from '../../api';

// Config
import localeData from '../../../public/locales/data.json';
import GoogleTagManager from '../../config/gtm';

// Utils
import lang from '../../utils/lang';

addLocaleData([es]);

let gtmInitialized = false;
// let ipSaved = false;

const withIntl = (Component) => {
  const Wrapper = ({ appState, appSetLangAction, ...props }) => {// eslint-disable-line
    let language = lang;

    if (appState.lang === '') {
      appSetLangAction(language);
    } else {
      language = appState.lang;

      setBaseUrlLang(language);
      if (!gtmInitialized) {
        GoogleTagManager.initialize(language);
        gtmInitialized = true;
      }
      // if (language === 'pt-BR' && !ipSaved) {
      //   setIPBR();
      //   ipSaved = true;
      // }
    }

    const messages = localeData[language.slice(0, 2)] || localeData.es;

    return (
      <IntlProvider defaultLocale="es-US" locale={language} messages={messages} textComponent={React.Fragment}>
        <Component {...props} />
      </IntlProvider>
    );
  };

  Wrapper.propTypes = {
    appState: PropTypes.object,// eslint-disable-line
    appSetLangAction: PropTypes.func,// eslint-disable-line
  };

  return Wrapper;
};

const mapStateToProps = state => ({
  appState: state.app,
});

const mapDispatchToProps = ({
  appSetLangAction,
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withIntl,
);
