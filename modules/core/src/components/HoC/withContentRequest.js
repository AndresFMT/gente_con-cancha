/* eslint-disable no-shadow */
// Libraries
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';

// Helpers
import isEmpty from 'lodash/isEmpty';
import get from 'lodash/get';

// Actions
import getContentAction from '../../actions/content';
import getHeaderAction from '../../actions/header';

const getRoute = (props) => {
  const url = get(props, 'match.url');
  if (url === '/') {
    return '/capitulo';
  }
  return url;
};

const withContentRequest = (Component) => {
  const { requestContent = true } = Component;

  const Wrapper = ({
    headerState, contentState, getContentAction, getHeaderAction, ...props
  }) => {
    const route = getRoute(props);

    const headerData = headerState.data || {};
    const contentData = contentState.data[route] || {};
    global.console.log(headerData);
    useEffect(() => {
      let action = false;
      if (isEmpty(headerData)) {
        getHeaderAction();
        action = true;
      }
      if (requestContent && isEmpty(contentData)) {
        getContentAction(route);
        action = true;
      }
      if (action) {
        try {
          window.scroll({
            top: 0,
            left: 0,
            behavior: 'smooth',
          });
        } catch (error) {
          window.scrollTo(0, 0);
        }
      }
    }, [route]);

    const loading = isEmpty(headerData)
      || (requestContent && isEmpty(contentData))
      || contentState.loading || headerState.loading;

    return (
      <Component {...props} headerData={headerData} contentData={contentData} loading={loading} />
    );
  };

  Wrapper.propTypes = {
    headerState: PropTypes.shape({
      data: PropTypes.shape({
        menuList: PropTypes.array,
      }),
      loading: PropTypes.bool,
    }).isRequired,
    contentState: PropTypes.shape({
      data: PropTypes.object,
      loading: PropTypes.bool,
    }).isRequired,
    getContentAction: PropTypes.func.isRequired,
    getHeaderAction: PropTypes.func.isRequired,
  };

  return Wrapper;
};

const mapStateToProps = state => ({
  contentState: state.content,
  headerState: state.header,
});

const mapDispatchToProps = ({
  getContentAction,
  getHeaderAction,
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withContentRequest,
);

export const contentDataShape = PropTypes.shape({
  data: PropTypes.object,
  loading: PropTypes.bool,
});

export const headerDataShape = PropTypes.shape({
  data: PropTypes.object,
  loading: PropTypes.bool,
});
