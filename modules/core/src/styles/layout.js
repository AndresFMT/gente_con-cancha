
export default [
  '414px', // Small
  '768px', // Medium
  '1024px', // Large
  '1367px', // Extra Large
  '100%', // FullWidth
];
