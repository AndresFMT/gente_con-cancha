import slidearrowleftfilled from '../icons/slidearrow-left-filled.svg';
import slidearrowrightfilled from '../icons/slidearrow-right-filled.svg';
import gallerycardfilled from '../icons/gallery-card-filled.svg';
import playerresting from '../icons/player-resting.svg';
import playerchapter from '../icons/play-capitulos.svg';
import videoplayfilled from '../icons/video-play-filled.svg';
import volumeonfilled from '../icons/volume-on-filled.svg';
import volumeofffilled from '../icons/volume-off-filled.svg';
import program from '../icons/program.svg';
import close from '../icons/close.svg';
import back from '../icons/back.svg';
import arrowWhite from '../icons/arrow-white.svg';
import resizefullscreen from '../icons/resize-full-screen.svg';

export default {
  slidearrowleftfilled,
  slidearrowrightfilled,
  gallerycardfilled,
  playerresting,
  playerchapter,
  videoplayfilled,
  volumeonfilled,
  volumeofffilled,
  program,
  close,
  back,
  arrowWhite,
  resizefullscreen,
};
