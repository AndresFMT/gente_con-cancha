export default {
  elevation1: '0 1px 2px 0 rgba(0,0,0,0.2), 0 1px 3px 0 rgba(0,0,0,0.1)',
  elevation2: '0 3px 6px 0 rgba(0,0,0,0.23), 0 3px 6px 0 rgba(0,0,0,0.16)',
  elevation3: '0 6px 6px 0 rgba(0,0,0,0.26), 0 10px 20px 0 rgba(0,0,0,0.19)',
  elevation4: '0 10px 10px 0 rgba(0,0,0,0.26), 0 14px 28px 0 rgba(0,0,0,0.25)',
  elevation5: '0 15px 12px 0 rgba(0,0,0,0.22), 0 19px 38px 0 rgba(0,0,0,0.3)',
};
