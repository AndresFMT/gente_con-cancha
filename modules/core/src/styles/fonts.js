export default {
  Kalam: 'Kalam',
  NeueMediumItalic: 'NeueMediumItalic',
  NeueMedium: 'NeueMedium',
  NeueLightItalic: 'NeueLightItalic',
  NeueLight: 'NeueLight',
  NeueItalic: 'NeueItalic',
  NeueBoldItalic: 'NeueBoldItalic',
  NeueBold: 'NeueBold',
  NeueRegular: 'NeueRegular',
};
