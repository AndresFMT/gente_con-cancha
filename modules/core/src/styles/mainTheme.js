import { createGlobalStyle } from 'styled-components';
import Kalam from '../../public/fonts/Kalam/Kalam-Bold.eot';

const globalColors = require('./colors');
const globalLayout = require('./layout');
const globalIcons = require('./icons');
const globalShadows = require('./shadows');
const globalFonts = require('./fonts');

const GlobalStyle = createGlobalStyle`
  @font-face {
    font-family: Kalam;
    src: url(${Kalam}) format('truetype');
    font-weight: normal;
    font-style: normal;
  }
  html {
    font-size: 10px;
    font-family: Kalam;
  }
`;

export default {
  breakpoints: globalLayout.default,
  fontSizes: [12, 14, 16, 20, 22, 24, 28, 32, 40, 44, 48, 64, 90],
  colors: globalColors.default,
  space: [0, 8, 16, 24, 32, 40, 48, 56],
  fonts: {
    sans1: 'Kalam',
    sans2: 'Kalam',
  },
  font: globalFonts.default,
  icons: globalIcons.default,
  shadows: globalShadows,
  GlobalStyle,
};
