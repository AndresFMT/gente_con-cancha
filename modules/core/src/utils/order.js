import get from 'lodash/get';
import find from 'lodash/find';

const getOrder = (component, components) => (
  find(components, comp => comp.component === component)
);

const setOrder = (component, components) => ({
  order: get(getOrder(component, components), 'order', 0),
});

export default setOrder;
