import 'url-search-params-polyfill';

const language = process.env.REACT_APP_LANG
|| (navigator.languages && navigator.languages[0])
|| navigator.language
|| navigator.userLanguage;

const searchParams = new URLSearchParams(window.location.search);

// eslint-disable-next-line import/no-mutable-exports
let lang = searchParams.get('lang') || language;

if (lang.includes('en')) {
  lang = lang.replace('en', 'es');
}

export default lang;
