import styled from 'styled-components';
import get from 'lodash/get';
import { Box } from 'rebass';

export const BoxBase = styled(Box)`
  max-width: ${props => get(props, 'theme.breakpoints.4', '1440px')};
  margin: 0 auto;
  width: 100%;
`;
