import TYPES from '../actions/header/types';

export const initialState = {
  data: {},
  loading: false,
  error: {},
};

const headerReducer = (state = initialState, { type, payload = null }) => {
  switch (type) {
    case TYPES.HEADER.SET.REQUEST:
      return {
        ...state,
        loading: true,
      };
    case TYPES.HEADER.SET.SUCCESS:
      return {
        ...state,
        loading: false,
        data: payload.data,
      };
    case TYPES.HEADER.SET.ERROR:
      return {
        ...state,
        loading: false,
        error: payload.error,
      };
    default:
      return state;
  }
};

export default headerReducer;
