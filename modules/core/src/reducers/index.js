import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import app from './app';
import content from './content';
import header from './header';

export default history => combineReducers({
  router: connectRouter(history),
  app,
  content,
  header,
});
