import { getHeader } from '../../api';
import {
  appFetchHeaderDataRequestAC,
  appFetchHeaderDataSuccessAC,
  appFetchHeaderDataErrorAC,
} from './actionCreators';

const appSetHeaderAction = () => async (dispatch) => {
  dispatch(appFetchHeaderDataRequestAC());
  try {
    const data = await getHeader();
    dispatch(appFetchHeaderDataSuccessAC(data.data));
  } catch (error) {
    dispatch(appFetchHeaderDataErrorAC(error));
  }
};

export default appSetHeaderAction;
