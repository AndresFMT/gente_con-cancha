import keyMirror from 'keymirror-nested';

export default keyMirror({
  HEADER: {
    SET: {
      REQUEST: null,
      SUCCESS: null,
      ERROR: null,
    },
  },
});
