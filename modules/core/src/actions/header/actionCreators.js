import TYPES from './types';

export const appFetchHeaderDataRequestAC = () => ({ type: TYPES.HEADER.SET.REQUEST });

export const appFetchHeaderDataSuccessAC = data => ({
  type: TYPES.HEADER.SET.SUCCESS,
  payload: { data },
});

export const appFetchHeaderDataErrorAC = error => ({
  type: TYPES.HEADER.SET.ERROR,
  payload: { error },
});
