import keyMirror from 'keymirror-nested';

export default keyMirror({
  CONTENT: {
    SET: {
      REQUEST: null,
      SUCCESS: null,
      ERROR: null,
    },
  },
});
