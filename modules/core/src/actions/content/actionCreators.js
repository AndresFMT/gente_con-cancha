import TYPES from './types';

export const appFetchContentDataRequestAC = () => ({ type: TYPES.CONTENT.SET.REQUEST });

export const appFetchContentDataSuccessAC = (data, route) => ({
  type: TYPES.CONTENT.SET.SUCCESS,
  payload: { data, route },
});

export const appFetchContentDataErrorAC = error => ({
  type: TYPES.CONTENT.SET.ERROR,
  payload: { error },
});
