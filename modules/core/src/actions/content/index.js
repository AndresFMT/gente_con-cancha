import { get } from '../../api';
import {
  appFetchContentDataRequestAC,
  appFetchContentDataSuccessAC,
  appFetchContentDataErrorAC,
} from './actionCreators';

const getContentAction = route => async (dispatch) => {
  dispatch(appFetchContentDataRequestAC());
  try {
    const data = await get(route);
    global.console.log(data.data);
    dispatch(appFetchContentDataSuccessAC(data.data, route));
  } catch (error) {
    dispatch(appFetchContentDataErrorAC(error));
  }
};

export default getContentAction;
