import TYPES from './types';

export const appSetLangAC = lang => ({ type: TYPES.APP.SETLANG, payload: { lang } });
