import {
  appSetLangAC,
} from './actionCreators';

export const appSetLangAction = lang => dispatch => dispatch(appSetLangAC(lang));
