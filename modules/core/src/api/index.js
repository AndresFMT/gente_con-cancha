import api from '../config/api';

export const get = async (route, ...props) => api.get(route, { ...props });

export const getHeader = async props => api.get('/menu', { ...props });

export const getHome = async props => api.get('/capitulo', { ...props });

export const getInfo = async (route, ...props) => api.get(route, { ...props });
