import React from 'react';
import { compose } from 'redux';
import { intlShape, injectIntl, defineMessages } from 'react-intl';
import isEmpty from 'lodash/isEmpty';

import { RenderHtml, Loading, ArticleLayout } from '@cancha/shared-components';
import { trackContent } from '@cancha/shared-components/utils/analytics';
import Page from '../../components/page';

import { BoxBase } from '../../utils/styled';

import withContentRequest, { contentDataShape, headerDataShape } from '../../components/HoC/withContentRequest';

const messages = defineMessages({
  titleSecond: {
    id: 'Home.TitleSecond',
    defaultMessage: 'Real entertainment and Lifestyle',
  },
  metaDescription: {
    id: 'Galeria.metaDescription',
    defaultMessage: 'Gente con Cacha',
  },
  metaKeywords: {
    id: 'Galeria.metaKeywords',
    defaultMessage: 'Gente con Cancha',
  },
});

const getFormattedMessages = intl => ({
  pageTitle: intl.formatMessage({
    id: 'Page.Title',
    defaultMessage: 'Gente con Cancha',
  }),
  titleSecond: intl.formatMessage(messages.titleSecond),
  metaDescription: intl.formatMessage(messages.metaDescription),
  metaKeywords: intl.formatMessage(messages.metaKeywords),
});

export const Extras = ({
  // eslint-disable-next-line react/prop-types
  contentData, headerData, intl, loading,
}) => {
  global.console.log(contentData.items);
  if (loading) {
    return <Loading type="home" />;
  }
  const {
    pageTitle, titleSecond, metaDescription, metaKeywords,
  } = getFormattedMessages(intl);

  if (!isEmpty(contentData.id)) {
    trackContent(contentData.id);
  }
  return (
    <Page
      id="extras"
      type="gradient"
      title={pageTitle}
      titleSecond={titleSecond}
      description={metaDescription}
      keywords={metaKeywords}
      menuList={headerData.menuList}
    >
      {(contentData) && (
        <BoxBase>
          <ArticleLayout
            data={contentData.items[0]}
            url={window.location}
          />
        </BoxBase>
      )}
      {(contentData && contentData.page && contentData.page.raw_html) && (
        <RenderHtml data={contentData.page.raw_html} />
      )}
    </Page>
  );
};

Extras.propTypes = {
  intl: intlShape.isRequired,
  contentData: contentDataShape.isRequired,
  headerData: headerDataShape.isRequired,
};

export default compose(
  withContentRequest,
  injectIntl,
)(Extras);
