import styled from 'styled-components';
import get from 'lodash/get';
import { Heading as HeadingBase, Box } from 'rebass';

export const Heading = styled(HeadingBase)`
  font-size: 30px;
  &:hover {
    background-color: ${props => get(props, 'theme.colors.transparent', '')}
  }
`;

export const BoxAds = styled(Box)`
  border-top: 1px solid  ${props => get(props, 'theme.colors.w1', '')};
  border-bottom: 1px solid  ${props => get(props, 'theme.colors.w1', '')};
`;

export const ChapterContent = styled(Box)`
  position: absolute;
`;
