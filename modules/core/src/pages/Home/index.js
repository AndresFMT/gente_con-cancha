/* eslint-disable react/prop-types */
import React from 'react';
import { compose } from 'redux';
import {
  intlShape, injectIntl, defineMessages,
} from 'react-intl';

// Helpers
// import isEmpty from 'lodash/isEmpty';

// Components
import {
  Loading,
  ChapterContent,
} from '@cancha/shared-components';
// import { trackContent } from '@cancha/shared-components/utils/analytics';

// Utils
import { BoxBase } from '../../utils/styled';

// Global Page
import Page from '../../components/page';

// HoC
import withContentRequest, { contentDataShape, headerDataShape } from '../../components/HoC/withContentRequest';

const messages = defineMessages({
  titleSecond: {
    id: 'Home.TitleSecond',
    defaultMessage: 'Real entertainment and Lifestyle',
  },
  metaDescription: {
    id: 'Home.metaDescription',
    defaultMessage: 'Gente con Cacha',
  },
  metaKeywords: {
    id: 'Home.metaKeywords',
    defaultMessage: 'Gente con Cancha',
  },
});

const getFormattedMessages = intl => ({
  pageTitle: intl.formatMessage({
    id: 'Page.Title',
    defaultMessage: 'Gente con Cancha',
  }),
  titleSecond: intl.formatMessage(messages.titleSecond),
  metaDescription: intl.formatMessage(messages.metaDescription),
  metaKeywords: intl.formatMessage(messages.metaKeywords),
});

const Home = ({
  contentData, headerData, loading, intl,
}) => {
  if (loading) {
    return <Loading type="home" />;
  }

  const {
    pageTitle, titleSecond, metaDescription, metaKeywords,
  } = getFormattedMessages(intl);

  global.console.log(contentData.id, 'yo');


  return (
    <Page
      id="capitulo"
      title={pageTitle}
      titleSecond={titleSecond}
      description={metaDescription}
      keywords={metaKeywords}
      type="gradient"
      menuList={headerData.menuList}
    >
      <ChapterContent items={contentData.items} />
    </Page>
  );
};

Home.propTypes = {
  intl: intlShape.isRequired,
  contentData: contentDataShape.isRequired,
  headerData: headerDataShape.isRequired,
};

export default compose(
  withContentRequest,
  injectIntl,
)(Home);
