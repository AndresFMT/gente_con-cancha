import React from 'react';
import { compose } from 'redux';
import { intlShape, injectIntl, defineMessages } from 'react-intl';

import isEmpty from 'lodash/isEmpty';


import {
  Loading,
  RenderHtml,
  SliderGallery,
} from '@cancha/shared-components';

import { trackContent } from '@cancha/shared-components/utils/analytics';
import Page from '../../components/page';

import { BoxBase } from '../../utils/styled';

import withContentRequest, { contentDataShape, headerDataShape } from '../../components/HoC/withContentRequest';


const messages = defineMessages({
  titleSecond: {
    id: 'Home.TitleSecond',
    defaultMessage: 'Real entertainment and Lifestyle',
  },
  metaDescription: {
    id: 'Galeria.metaDescription',
    defaultMessage: 'Gente con Cacha',
  },
  metaKeywords: {
    id: 'Galeria.metaKeywords',
    defaultMessage: 'Gente con Cancha',
  },
});

const getFormattedMessages = intl => ({
  pageTitle: intl.formatMessage({
    id: 'Page.Title',
    defaultMessage: 'Gente con Cancha',
  }),
  titleSecond: intl.formatMessage(messages.titleSecond),
  metaDescription: intl.formatMessage(messages.metaDescription),
  metaKeywords: intl.formatMessage(messages.metaKeywords),
});

const Galerias = ({
  // eslint-disable-next-line react/prop-types
  contentData, headerData, intl, loading,
}) => {
  if (loading) {
    return <Loading type="home" />;
  }

  const {
    pageTitle, titleSecond, metaDescription, metaKeywords,
  } = getFormattedMessages(intl);

  if (!isEmpty(contentData.id)) {
    trackContent(contentData.id);
  }
  const imagesGallery = contentData.items[0].images;
  return (
    <Page
      id="galerias"
      type="gradient"
      title={pageTitle}
      titleSecond={titleSecond}
      description={metaDescription}
      keywords={metaKeywords}
      menuList={headerData.menuList}
    >
      {(contentData) && (
        <BoxBase>
          <SliderGallery sliderContent={imagesGallery} />
        </BoxBase>
      )}
      {(contentData && contentData.page && contentData.page.raw_html) && (
        <RenderHtml data={contentData.page.raw_html} />
      )}
    </Page>
  );
};

Galerias.propTypes = {
  intl: intlShape.isRequired,
  contentData: contentDataShape.isRequired,
  headerData: headerDataShape.isRequired,
};

export default compose(
  withContentRequest,
  injectIntl,
)(Galerias);
